"A module that provides utilities for the package."
from contextlib import contextmanager
from time import time

from absl import logging


@contextmanager
def time_it(desc: str) -> None:
    start = time()
    yield
    logging.info(f"{desc} takes {time() - start: 1.4f} s to complete")
