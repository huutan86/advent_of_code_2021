import math
import re


def explode3(y):
    x = str(y)
    depth = 0
    match_string = []
    for i, char in enumerate(x):
        if depth <= 4:
            if char == "[":
                depth = depth + 1
            elif char == "]":
                depth = depth - 1
        elif char != "]":
            match_string.append(i)
        else:
            break
    if match_string:

        final_match = x[match_string[0] - 1 : match_string[-1] + 2]
        a, b = eval(final_match)
        return a, b, x[: match_string[0] - 1] + "-9999" + x[match_string[-1] + 2 :]
    else:
        return None, None, x


def process_explosion(y, left_num, right_num):
    def myaddleft(m):
        # print(m)
        return str(int(m.group(0)) + left_num)

    def myaddright(m):
        # print(m)
        return str(int(m.group(0)) + right_num)

    ystr = str(y)
    i_min = ystr.find("-9999")
    substr = ystr[:i_min]
    new_sub_left = re.sub("\d+(?=\D*$)", myaddleft, substr)
    substl = ystr[i_min + 5 :]

    new_right_str = re.sub("\d+", myaddright, substl, count=1)

    return new_sub_left + "0" + new_right_str


def full_explosion(y):
    a, b, y = explode3(y)
    if a is None:
        return eval(y)
    return eval(process_explosion(y, a, b))


def split(x):
    def subsplit(y):
        i = int(y.group(0))
        return f"[{math.floor(int(i) / 2)},{math.ceil(i/2)}]"

    xstr = str(x)
    return eval(re.sub(r"\d{2}", subsplit, xstr, count=1))


def reduce(x):
    out = full_explosion(x)
    if out == x:
        out = split(x)
    return out


def full_reduce(my_test_after_add):
    next_iter = my_test_after_add
    while True:
        next_iter = reduce(next_iter)
        print(next_iter)
        # print(type(next_iter),type(reduce(next_iter)))
        if next_iter == reduce(next_iter):
            break

    return next_iter


def add_fish(a, b):
    return [a, b]


def magnitude(s):
    a = eval(s)
    return str(a[0] * 3 + a[1] * 2)


def more_magnitude(x):
    def f(z):
        # print(z.group(0))
        return magnitude(z.group(0))

    y = str(x)
    # print(y)
    out = re.sub(r"\[\d+, \d+\]", f, y)
    try:
        return int(out)
    except:
        return eval(out)


def final_mag(y):
    next_iter = y
    while True:
        y = more_magnitude(y)
        if isinstance(y, int):
            break

    return y


from pathlib import Path

day18_input_file_name = Path(__file__).parent.parent / "data" / "day18_input_test.txt"
with open(day18_input_file_name) as f:
    test_input = f.readlines()

list_of_numbers = [eval(x) for x in test_input]

running_sum = list_of_numbers.pop(0)
while list_of_numbers:
    next_add = list_of_numbers.pop(0)
    running_sum = add_fish(running_sum, next_add)
    running_sum = full_reduce(running_sum)

final_mag(running_sum)

from tqdm.notebook import tqdm

list_of_numbers = [eval(x) for x in test_input]

import itertools

all_res = []
for a, b in tqdm(itertools.combinations(list_of_numbers, 2)):
    all_res.append(final_mag(full_reduce(add_fish(a, b))))
    all_res.append(final_mag(full_reduce(add_fish(b, a))))

max(all_res)
