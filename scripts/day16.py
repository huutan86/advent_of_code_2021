from functools import reduce
from operator import mul
from pathlib import Path
from typing import List
from typing import Optional
from typing import Tuple

from absl import logging

from scripts._util import time_it


class Packet:
    def __init__(self, version: int, type_id: int, val: Optional[int], child_packets: List["Packet"] = []):
        self._version = version
        self._type_id = type_id
        self._val = val
        self._child_packets = child_packets

    @property
    def version_sum(self) -> int:
        if len(self._child_packets) == 0:
            return self._version
        else:
            return self._version + sum(child.version_sum for child in self._child_packets)

    @property
    def value(self) -> int:
        if self._type_id == 0:
            return sum(child.value for child in self._child_packets)
        elif self._type_id == 1:
            return reduce(mul, [child.value for child in self._child_packets])
        elif self._type_id == 2:
            return min(child.value for child in self._child_packets)
        elif self._type_id == 3:
            return max(child.value for child in self._child_packets)
        elif self._type_id == 4:
            return self._val
        elif self._type_id == 5:
            return 1 if self._child_packets[0].value > self._child_packets[1].value else 0
        elif self._type_id == 6:
            return 1 if self._child_packets[0].value < self._child_packets[1].value else 0
        elif self._type_id == 7:
            return 1 if self._child_packets[0].value == self._child_packets[1].value else 0


def day16() -> None:
    day16_input_file_name = Path(__file__).parent.parent / "data" / "day16_input.txt"
    with open(day16_input_file_name, "r") as file:
        message = file.readline().strip()
    bin_message = _convert_to_binary_str(message)
    all_packets = _parse_message(bin_message)

    with time_it("part 1") as _:
        _part1(all_packets)

    with time_it("part 2") as _:
        _part2(all_packets)


def _convert_to_binary_str(hex_str: int) -> str:
    return "".join(bin(int(x, 16))[2:].zfill(4) for x in hex_str)


def _parse_message(message: str) -> List[Packet]:
    idx = 0
    message_len = len(message)
    all_packets = []
    while idx < message_len:
        new_packet, packet_len = _parse_single_packet(message[idx:])
        if new_packet is not None:
            all_packets.append(new_packet)
        idx += packet_len
    return all_packets


def _parse_single_packet(message: str) -> Tuple[Optional[Packet], int]:
    # Handle the trailing zeros.
    if set(list(message)) == {"0"}:
        return None, len(message)

    type_id = int(message[3:6], 2)
    if type_id == 4:
        return _parse_literal_packet(message)
    else:
        return _parse_operator_packet(message)


def _parse_literal_packet(message) -> Tuple[Packet, int]:
    """Returns a literal message and the total length of the message."""
    idx = 0
    version = int(message[idx : idx + 3], 2)
    idx += 3
    type_id = int(message[idx : idx + 3], 2)
    idx += 3

    assert type_id == 4
    literal_str = ""
    while True:
        substr = message[idx : idx + 5]
        idx += 5
        literal_str += substr[1:]
        if substr[0] == "0":
            break

    return Packet(version=version, type_id=type_id, val=int(literal_str, 2), child_packets=[]), idx


def _parse_operator_packet(message: str) -> Tuple[Packet, int]:
    """Returns a operator packet and the total length of the message."""
    idx = 0
    version = int(message[idx : idx + 3], 2)
    idx += 3
    type_id = int(message[idx : idx + 3], 2)
    idx += 3
    assert type_id != 4
    bit_label = message[idx]
    idx += 1
    if bit_label == "0":
        subpacket_len = int(message[idx : idx + 15], 2)
        idx += 15

        # Parse all the child packets
        child_packets = []
        total_child_packet_len = 0
        while total_child_packet_len < subpacket_len:
            sub_packet, sub_packet_len = _parse_single_packet(message[idx:])
            idx += sub_packet_len
            child_packets.append(sub_packet)
            total_child_packet_len += sub_packet_len
    else:
        num_packets = int(message[idx : idx + 11], 2)
        idx += 11

        # Parse all child packets
        child_packets = []
        for _ in range(num_packets):
            sub_packet, sub_packet_len = _parse_single_packet(message[idx:])
            idx += sub_packet_len
            child_packets.append(sub_packet)

    return Packet(version=version, type_id=type_id, val=None, child_packets=child_packets), idx


def _part1(all_packets: List[Packet]) -> None:
    """
    --- Day 16: Packet Decoder ---
    As you leave the cave and reach open waters, you receive a transmission from the Elves back on the ship.

    The transmission was sent using the Buoyancy Interchange Transmission System (BITS), a method of packing numeric
    expressions into a binary sequence. Your submarine's computer has saved the transmission in hexadecimal (your puzzle
     input).

    The first step of decoding the message is to convert the hexadecimal representation into binary. Each character of
    hexadecimal corresponds to four bits of binary data:

    0 = 0000
    1 = 0001
    2 = 0010
    3 = 0011
    4 = 0100
    5 = 0101
    6 = 0110
    7 = 0111
    8 = 1000
    9 = 1001
    A = 1010
    B = 1011
    C = 1100
    D = 1101
    E = 1110
    F = 1111
    The BITS transmission contains a single packet at its outermost layer which itself contains many other packets. The
    hexadecimal representation of this packet might encode a few extra 0 bits at the end; these are not part of the
    transmission and should be ignored.

    Every packet begins with a standard header: the first three bits encode the packet version, and the next three bits
    encode the packet type ID. These two values are numbers; all numbers encoded in any packet are represented as binary
    with the most significant bit first. For example, a version encoded as the binary sequence 100 represents the number
     4.

    Packets with type ID 4 represent a literal value. Literal value packets encode a single binary number. To do this,
    the binary number is padded with leading zeroes until its length is a multiple of four bits, and then it is broken
    into groups of four bits. Each group is prefixed by a 1 bit except the last group, which is prefixed by a 0 bit.
    These groups of five bits immediately follow the packet header. For example, the hexadecimal string D2FE28 becomes:

    110100101111111000101000
    VVVTTTAAAAABBBBBCCCCC
    Below each bit is a label indicating its purpose:

    The three bits labeled V (110) are the packet version, 6.
    The three bits labeled T (100) are the packet type ID, 4, which means the packet is a literal value.
    The five bits labeled A (10111) start with a 1 (not the last group, keep reading) and contain the first four bits of
    the number, 0111.
    The five bits labeled B (11110) start with a 1 (not the last group, keep reading) and contain four more bits of the
    number, 1110.
    The five bits labeled C (00101) start with a 0 (last group, end of packet) and contain the last four bits of the
    number, 0101.
    The three unlabeled 0 bits at the end are extra due to the hexadecimal representation and should be ignored.
    So, this packet represents a literal value with binary representation 011111100101, which is 2021 in decimal.

    Every other type of packet (any packet with a type ID other than 4) represent an operator that performs some
    calculation on one or more sub-packets contained within. Right now, the specific operations aren't important; focus
    on parsing the hierarchy of sub-packets.

    An operator packet contains one or more packets. To indicate which subsequent binary data represents its sub-packets
    , an operator packet can use one of two modes indicated by the bit immediately after the packet header; this is
    called the length type ID:

    If the length type ID is 0, then the next 15 bits are a number that represents the total length in bits of the sub-
    packets contained by this packet.
    If the length type ID is 1, then the next 11 bits are a number that represents the number of sub-packets immediately
     contained by this packet.
    Finally, after the length type ID bit and the 15-bit or 11-bit field, the sub-packets appear.

    For example, here is an operator packet (hexadecimal string 38006F45291200) with length type ID 0 that contains two
    sub-packets:

    00111000000000000110111101000101001010010001001000000000
    VVVTTTILLLLLLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBBBBBB
    The three bits labeled V (001) are the packet version, 1.
    The three bits labeled T (110) are the packet type ID, 6, which means the packet is an operator.
    The bit labeled I (0) is the length type ID, which indicates that the length is a 15-bit number representing the
    number of bits in the sub-packets.
    The 15 bits labeled L (000000000011011) contain the length of the sub-packets in bits, 27.
    The 11 bits labeled A contain the first sub-packet, a literal value representing the number 10.
    The 16 bits labeled B contain the second sub-packet, a literal value representing the number 20.
    After reading 11 and 16 bits of sub-packet data, the total length indicated in L (27) is reached, and so parsing of
    this packet stops.

    As another example, here is an operator packet (hexadecimal string EE00D40C823060) with length type ID 1 that
    contains three sub-packets:

    11101110000000001101010000001100100000100011000001100000
    VVVTTTILLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBCCCCCCCCCCC
    The three bits labeled V (111) are the packet version, 7.
    The three bits labeled T (011) are the packet type ID, 3, which means the packet is an operator.
    The bit labeled I (1) is the length type ID, which indicates that the length is a 11-bit number representing the
    number of sub-packets.
    The 11 bits labeled L (00000000011) contain the number of sub-packets, 3.
    The 11 bits labeled A contain the first sub-packet, a literal value representing the number 1.
    The 11 bits labeled B contain the second sub-packet, a literal value representing the number 2.
    The 11 bits labeled C contain the third sub-packet, a literal value representing the number 3.
    After reading 3 complete sub-packets, the number of sub-packets indicated in L (3) is reached, and so parsing of
    this packet stops.

    For now, parse the hierarchy of the packets throughout the transmission and add up all of the version numbers.

    Here are a few more examples of hexadecimal-encoded transmissions:

    8A004A801A8002F478 represents an operator packet (version 4) which contains an operator packet (version 1) which
    contains an operator packet (version 5) which contains a literal value (version 6); this packet has a version sum of
    16.
    620080001611562C8802118E34 represents an operator packet (version 3) which contains two sub-packets; each sub-packet
     is an operator packet that contains two literal values. This packet has a version sum of 12.
    C0015000016115A2E0802F182340 has the same structure as the previous example, but the outermost packet uses a
    different length type ID. This packet has a version sum of 23.
    A0016C880162017C3686B18A3D4780 is an operator packet that contains an operator packet that contains an operator
    packet that contains five literal values; it has a version sum of 31.
    Decode the structure of your hexadecimal-encoded BITS transmission; what do you get if you add up the version
    numbers in all packets?

    Your puzzle answer was 913.
    """
    version_count = sum(packet.version_sum for packet in all_packets)
    logging.info(f"p1 res = {version_count}.")


def _part2(all_packets: List[Packet]) -> None:
    """
    --- Part Two ---
    Now that you have the structure of your transmission decoded, you can calculate the value of the expression it
    represents.

    Literal values (type ID 4) represent a single number as described above. The remaining type IDs are more interesting:

    Packets with type ID 0 are sum packets - their value is the sum of the values of their sub-packets. If they only
    have a single sub-packet, their value is the value of the sub-packet.
    Packets with type ID 1 are product packets - their value is the result of multiplying together the values of their
    sub-packets. If they only have a single sub-packet, their value is the value of the sub-packet.
    Packets with type ID 2 are minimum packets - their value is the minimum of the values of their sub-packets.
    Packets with type ID 3 are maximum packets - their value is the maximum of the values of their sub-packets.
    Packets with type ID 5 are greater than packets - their value is 1 if the value of the first sub-packet is greater
    than the value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-
    packets.
    Packets with type ID 6 are less than packets - their value is 1 if the value of the first sub-packet is less than
    the value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-packets.
    Packets with type ID 7 are equal to packets - their value is 1 if the value of the first sub-packet is equal to the
    value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-packets.
    Using these rules, you can now work out the value of the outermost packet in your BITS transmission.

    For example:

    C200B40A82 finds the sum of 1 and 2, resulting in the value 3.
    04005AC33890 finds the product of 6 and 9, resulting in the value 54.
    880086C3E88112 finds the minimum of 7, 8, and 9, resulting in the value 7.
    CE00C43D881120 finds the maximum of 7, 8, and 9, resulting in the value 9.
    D8005AC2A8F0 produces 1, because 5 is less than 15.
    F600BC2D8F produces 0, because 5 is not greater than 15.
    9C005AC2F8F0 produces 0, because 5 is not equal to 15.
    9C0141080250320F1802104A08 produces 1, because 1 + 3 = 2 * 2.
    What do you get if you evaluate the expression represented by your hexadecimal-encoded BITS transmission?

    Your puzzle answer was 1510977819698.
    """
    logging.info(f"p2 res = {all_packets[0].value}.")
