"""Solution to day 24.
"""
import itertools
from pathlib import Path
from typing import List

from scripts._util import time_it


class State:
    def __init__(self, x: int, y: int, z: int, w: int):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def __hash__(self):
        return hash((self.x, self.y, self.z, self.w))

    def __getattr__(self, item):
        if item in self.__dict__:
            return self[item]
        else:
            raise AttributeError

    def __setattr__(self, key, value):
        self.__dict__[key] = value

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __repr__(self):
        return f"State(x={self.x}, y={self.y}, z={self.z}, w={self.w})"

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, key, value):
        return setattr(self, key, value)


def day24() -> None:
    day24_input_file_name = Path(__file__).parent.parent / "data" / "day24_input.txt"
    with open(day24_input_file_name, "r") as file:
        commands = [l.strip().split() for l in file.readlines()]

    with time_it("part 1") as _:
        _all_parts(commands)


def _all_parts(commands: List[List[str]]) -> None:
    """
    Note that the problem was divided into 14 blocks, each of them have the following shape:
    inp w
    mul x 0
    add x z
    mod x 26
    div z {div}
    add x {a1}
    eql x w
    eql x 0
    mul y 0
    add y 25
    mul y x
    add y 1
    mul z y
    mul y 0
    add y w
    add y {a2}
    mul y x
    add z y

    where we have (div, a1, a2)
    be equals to:
    (1, 15, 13),
    (1, 10, 16),
    (1, 12, 2),
    (1, 10, 8),
    (1, 14, 11),
    (26, -11, 6),
    (1, 10, 12),
    (26, -16, 2),
    (26, -9, 2),
    (1, 11, 15),
    (26, -8, 1),
    (26, -8, 10))
    (26, -10, 14),
    (26, -9, 10),

    What the block was doing is:
    1/ Compute:
        x = (z % 26) + a1
        z = z // {div}
    2/ Compare x or (z (before div) % 26) + a1 with the input w (the next two 'eql'. If they are the same, assign x = 0, else x = 1
    3/ Swt y to 25. If the comparison above is equal, set y to 1, else to 26.
    4/ If the comparison is the same, keep z unchanged, else, return z = 26 * z + w + a2
    For each block, x, y, w is reset. Only the value of z is propagated.

    Let z_(n-1) be the signal passed fromm the previous block.
    When when div == 1, because x % 26 is a non-negative number and a1 >= 10, hence. we equality condition
    can't happen. Therefore,
        z_n = 26 * z_(n-1) + w + a2. This increases the value of z by O(26).

    If div == 26, a1 is negative. Then, if (z_n % 26) + a1 == w,
        z_n = z_(n-1) // 26
    else:
        z_n = z_(n-1) + w + a2.

    We have the Combining these relation, we have:
    z0 = z (input condition)

    z1 = 26 * z0 + w0 + 13
    z2 = 26 * z1 + w1 + 16
    z3 = 26 * z2 + w2 + 2
    z4 = 26 * z3 + w3 + 8
    z5 = 26 * z4 + w4 + 11
    z6 = z5 // 26 = z4 + (w4 + 11) // 26 = z4 with the condition that z_5 % 26 - 11 == w5 or (w4 + 11) - 11 = w5
    z7 = 26 * z6 + w6 + 12
    z8 = z7 // 26 = z6 = z4 with the condition that z7 % 26 - 16 = w7 or (w6 + 12) - 16 = w7
    z9 = z8 // 26 6 = z4 // 26 = z3 with the condition that z_8 % 26 - 9 = w8 or (w3 + 8) - 9 = w8
    z10 = 26 * z9 + w9 + 15
    z11 = z10 // 26 = z9 with the condition that z10 % 26 - 8 = w10 or (w9 + 15) - 8 = w10
    z12 = z11 // 26 = z2 with the condition that z11 % 26 - 8 = w11 or (w2 + 2) - 8 = w11
    z13 = z12 // 26 = z1 with the condition that z12 % 26 - 10 = w12 or (w1 + 16) - 10 = w12
    z14 = z13 // 26 = z with the condition that z13 % 26 - 9 = w13 or (w0 + 13) - 9 = w13

    This is because there were 7 times that z increase the values, there must be 7 times that z decreases the values.

    Simplifying these relations + eliminate redundant variables, we have:
    z1 = 26 * z + w0 + 13
    z2 = 26 * z1 + w1 + 16
    z3 = 26 * z2 + w2 + 2
    z4 = 26 * z3 + w3 + 8
    z5 = 26 * z4 + w4 + 11
    z7 = 26 * z4 + w6 + 12
    z10 = 26 * z3 + w9 + 15

    w5 = w4
    w7 = w6 - 4
    w8 = w3 - 1
    w10 = w9 + 7
    w11 = w2 - 6
    w12 = w1 + 6
    w13 = w0 + 4
    z % 26 - 9 = w13 = z - 9
    and 0<=z <26.

    Independent variables: z, w0, w1, w2, w3, w4, w6, w9,
    because w10 <= 9 => w9 <= 2

    Also, w11<=9 => but w1 >= 9 b/c w11 = w1 + 8.
    Hence,
        w11 = 9
        w1 = 1

    To further constraints the search range, we must have:
    1<= w13 <=9. Hence 10 <= z <= 18
    w6 = w7 + 4 => 5<=w6 <=9
    w3 = w8 + 1 => w3 >= 2
    w11 = w2 - 6 >= 1 => w2 >=7
    w12 = w1 + 6 <= 9 => w1 <= 3
    w13 = w0 + 4 <= 9 => w0 <= 5
    """
    sols = []
    z = 0
    for w0, w1, w2, w3, w4, w6, w9 in itertools.product(
        range(1, 6), range(1, 4), range(7, 10), range(2, 10), range(1, 10), range(5, 10), range(1, 3)
    ):
        z1 = 26 * z + w0 + 13
        z2 = 26 * z1 + w1 + 16
        z3 = 26 * z2 + w2 + 2
        z4 = 26 * z3 + w3 + 8
        z5 = 26 * z4 + w4 + 11
        w10 = w9 + 7
        w11 = w2 - 6
        w5 = w4
        w7 = w6 - 4
        w8 = w3 - 1
        w12 = w1 + 6
        w13 = w0 + 4
        if not (
            1 <= w5 <= 9
            and 1 <= w7 <= 9
            and 1 <= w8 <= 9
            and 1 <= w10 <= 9
            and 1 <= w11 <= 9
            and 1 <= w12 <= 9
            and 1 <= w13
        ):
            continue

        new_num = int(
            str(w0)
            + str(w1)
            + str(w2)
            + str(w3)
            + str(w4)
            + str(w5)
            + str(w6)
            + str(w7)
            + str(w8)
            + str(w9)
            + str(w10)
            + str(w11)
            + str(w12)
            + str(w13)
        )
        sols.append(new_num)

    print(f"P1 res = {max(sols)}")
    print(f"P2 res = {min(sols)}")
