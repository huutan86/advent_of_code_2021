"""Solution to day 18 - Binary tree"""
from pathlib import Path
from typing import Any
from typing import List
from typing import Optional
from typing import Tuple
from typing import Union

from absl import logging

from scripts._util import time_it


def day18() -> None:
    day18_input_file_name = Path(__file__).parent.parent / "data" / "day18_input.txt"
    with open(day18_input_file_name, "r") as file:
        all_lines = [l.strip() for l in file.readlines()]

    with time_it("part 1") as _:
        _part1(all_lines)

    with time_it("part 2") as _:
        _part2(all_lines)


class Node:
    """A node that hold references to other node in the tree"""

    def __init__(
        self,
        left: Optional["Node"] = None,
        right: Optional["Node"] = None,
        parent: Optional["Node"] = None,
        level: int = 0,
        value: Optional[int] = None,
    ):
        self.left = left
        self.right = right
        self.parent = parent
        self.value = value
        self.level = level

    def left_and_right_nodes_have_values(self) -> True:
        return self.left.value is not None and self.right.value is not None

    def __repr__(self):
        """Returns the presentation of a node."""
        out_str = ""
        if self.value is not None:
            out_str += str(self.value)
        else:
            out_str += "[" + repr(self.left) + "," + repr(self.right) + "]"
        return out_str

    def is_left_node(self):
        return self == self.parent.left

    def is_right_node(self):
        return self == self.parent.right

    def is_leaf_node(self):
        return self.value is not None


def _create_node_from_data(data: Union[int, List[List[Any]]], parent_node: Optional[Node], level: int) -> Node:
    if isinstance(data, int):
        return Node(left=None, right=None, parent=parent_node, value=data, level=level)
    elif isinstance(data, list):
        assert len(data) == 2
        node = Node(left=None, right=None, parent=parent_node, value=None, level=level)
        node.left = _create_node_from_data(data[0], parent_node=node, level=level + 1)
        node.right = _create_node_from_data(data[1], parent_node=node, level=level + 1)
        return node


def _reduce(node: Node) -> Node:
    node, _, _ = _explode(node)
    while True:
        if _find_node_to_split(node) is not None:
            node = _split_node(node)
            node, _, _ = _explode(node)
        else:
            break
    return node


def _explode(node: Node) -> Tuple[Optional[Node], Optional[int], Optional[int]]:
    if node.is_leaf_node():
        return node, None, None
    elif node.left_and_right_nodes_have_values():
        if node.level < 4:
            return node, None, None
        else:
            left_val = node.left.value
            right_val = node.right.value
            new_node = Node(left=None, right=None, value=0, parent=node.parent, level=node.level)
            return new_node, left_val, right_val
    else:
        _process_child_node(node.left)
        _process_child_node(node.right)
        return node, None, None


def _process_child_node(node: Node) -> None:
    new_node, left_val, right_val = _explode(node)
    nearest_leaf_node_to_the_left = _find_nearest_leaf_node_to_the_left(node)
    if nearest_leaf_node_to_the_left is not None and left_val is not None:
        nearest_leaf_node_to_the_left.value += left_val

    nearest_leaf_node_to_the_right = _find_nearest_leaf_node_to_the_right(node)
    if nearest_leaf_node_to_the_right is not None and right_val is not None:
        nearest_leaf_node_to_the_right.value += right_val

    if node.parent is not None and node.is_left_node():
        node.parent.left = new_node
    elif node.parent is not None and node.is_right_node():
        node.parent.right = new_node


def _find_nearest_leaf_node_to_the_left(node: Node) -> Optional[Node]:
    """Find the nearest right leaf node on the left of the current node.

    Going up the tree until we see a parent that have us on the right branch. Once found, go left once and keep going
    right until we see the leaf node.
    """
    cur_node = node
    while cur_node.parent is not None and cur_node.is_left_node():
        cur_node = cur_node.parent
        if cur_node is None:
            return None

        # cur_node is root node, can't go further
        if cur_node.parent is None:
            return None

    # At the right parent.
    cur_node = cur_node.parent.left
    while not cur_node.is_leaf_node():
        cur_node = cur_node.right
    return cur_node


def _find_nearest_leaf_node_to_the_right(node: Node) -> Optional[Node]:
    """Find the nearest right leaf node compared to the current mode.

    Going up the tree until we see a parent that have us on the left branch. Once found, go right once, and keep going
    left until we see the leaf node.

    """
    cur_node = node
    while cur_node.parent is not None and cur_node.is_right_node():
        cur_node = cur_node.parent
        if cur_node is None:
            return None

        # cur_node is root node, can't go further
        if cur_node.parent is None:
            return None

    cur_node = cur_node.parent.right
    while not cur_node.is_leaf_node():
        cur_node = cur_node.left
    return cur_node


def _find_node_to_split(node: Node) -> Optional[Node]:
    if node.is_leaf_node():
        if node.value >= 10:
            return node
        else:
            return None
    else:
        left_res = _find_node_to_split(node.left)
        if left_res is not None:
            return left_res

        right_res = _find_node_to_split(node.right)
        if right_res is not None:
            return right_res

        return None


def _split_node(node: Node) -> Node:
    """Use DFS to find a node to split."""
    node_to_split_res = _find_node_to_split(node)
    if node_to_split_res is not None:
        left_val = node_to_split_res.value // 2
        right_val = node_to_split_res.value - left_val
        is_current_node_left = node_to_split_res.is_left_node()
        new_node = Node(
            left=None, right=None, parent=node_to_split_res.parent, value=None, level=node_to_split_res.level
        )
        new_node.left = _create_node_from_data(left_val, parent_node=new_node, level=node_to_split_res.level + 1)
        new_node.right = _create_node_from_data(right_val, parent_node=new_node, level=node_to_split_res.level + 1)
        if is_current_node_left:
            node_to_split_res.parent.left = new_node
        else:
            node_to_split_res.parent.right = new_node
    return node


def _part1(all_lines: List[str]) -> None:
    """
    --- Day 18: Snailfish ---
    You descend into the ocean trench and encounter some snailfish. They say they saw the sleigh keys! They'll even tell
     you which direction the keys went if you help one of the smaller snailfish with his math homework.

    Snailfish numbers aren't like regular numbers. Instead, every snailfish number is a pair - an ordered list of two
    elements. Each element of the pair can be either a regular number or another pair.

    Pairs are written as [x,y], where x and y are the elements within the pair. Here are some example snailfish numbers,
     one snailfish number per line:

    [1,2]
    [[1,2],3]
    [9,[8,7]]
    [[1,9],[8,5]]
    [[[[1,2],[3,4]],[[5,6],[7,8]]],9]
    [[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
    [[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]
    This snailfish homework is about addition. To add two snailfish numbers, form a pair from the left and right
    parameters of the addition operator. For example, [1,2] + [[3,4],5] becomes [[1,2],[[3,4],5]].

    There's only one problem: snailfish numbers must always be reduced, and the process of adding two snailfish numbers
    can result in snailfish numbers that need to be reduced.

    To reduce a snailfish number, you must repeatedly do the first action in this list that applies to the snailfish
    number:

    If any pair is nested inside four pairs, the leftmost such pair explodes.
    If any regular number is 10 or greater, the leftmost such regular number splits.
    Once no action in the above list applies, the snailfish number is reduced.

    During reduction, at most one action applies, after which the process returns to the top of the list of actions. For
    example, if split produces a pair that meets the explode criteria, that pair explodes before other splits occur.

    To explode a pair, the pair's left value is added to the first regular number to the left of the exploding pair (if
    any), and the pair's right value is added to the first regular number to the right of the exploding pair (if any).
    Exploding pairs will always consist of two regular numbers. Then, the entire exploding pair is replaced with the
    regular number 0.

    Here are some examples of a single explode action:

    [[[[[9,8],1],2],3],4] becomes [[[[0,9],2],3],4] (the 9 has no regular number to its left, so it is not added to any
    regular number).
    [7,[6,[5,[4,[3,2]]]]] becomes [7,[6,[5,[7,0]]]] (the 2 has no regular number to its right, and so it is not added to
    any regular number).
    [[6,[5,[4,[3,2]]]],1] becomes [[6,[5,[7,0]]],3].
    [[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]] becomes [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]] (the pair [3,2] is unaffected
    because the pair [7,3] is further to the left; [3,2] would explode on the next action).
    [[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]] becomes [[3,[2,[8,0]]],[9,[5,[7,0]]]].
    To split a regular number, replace it with a pair; the left element of the pair should be the regular number divided
    by two and rounded down, while the right element of the pair should be the regular number divided by two and rounded
    up. For example, 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6], and so on.

    Here is the process of finding the reduced result of [[[[4,3],4],4],[7,[[8,4],9]]] + [1,1]:

    after addition: [[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]
    after explode:  [[[[0,7],4],[7,[[8,4],9]]],[1,1]]
    after explode:  [[[[0,7],4],[15,[0,13]]],[1,1]]
    after split:    [[[[0,7],4],[[7,8],[0,13]]],[1,1]]
    after split:    [[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]
    after explode:  [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
    Once no reduce actions apply, the snailfish number that remains is the actual result of the addition operation:
    [[[[0,7],4],[[7,8],[6,0]]],[8,1]].

    The homework assignment involves adding up a list of snailfish numbers (your puzzle input). The snailfish numbers
    are each listed on a separate line. Add the first snailfish number and the second, then add that result and the
    third, then add that result and the fourth, and so on until all numbers in the list have been used once.

    For example, the final sum of this list is [[[[1,1],[2,2]],[3,3]],[4,4]]:

    [1,1]
    [2,2]
    [3,3]
    [4,4]
    The final sum of this list is [[[[3,0],[5,3]],[4,4]],[5,5]]:

    [1,1]
    [2,2]
    [3,3]
    [4,4]
    [5,5]
    The final sum of this list is [[[[5,0],[7,4]],[5,5]],[6,6]]:

    [1,1]
    [2,2]
    [3,3]
    [4,4]
    [5,5]
    [6,6]
    Here's a slightly larger example:

    [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
    [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
    [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
    [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
    [7,[5,[[3,8],[1,4]]]]
    [[2,[2,2]],[8,[8,1]]]
    [2,9]
    [1,[[[9,3],9],[[9,0],[0,7]]]]
    [[[5,[7,4]],7],1]
    [[[[4,2],2],6],[8,7]]
    The final sum [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] is found after adding up the above snailfish
    numbers:

      [[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
    + [7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
    = [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]

      [[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]
    + [[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
    = [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]

      [[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]
    + [[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
    = [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]

      [[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]
    + [7,[5,[[3,8],[1,4]]]]
    = [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]

      [[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]
    + [[2,[2,2]],[8,[8,1]]]
    = [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]

      [[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]
    + [2,9]
    = [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]

      [[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]
    + [1,[[[9,3],9],[[9,0],[0,7]]]]
    = [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]

      [[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]
    + [[[5,[7,4]],7],1]
    = [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]

      [[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]
    + [[[[4,2],2],6],[8,7]]
    = [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]
    To check whether it's the right answer, the snailfish teacher only checks the magnitude of the final sum. The
    magnitude of a pair is 3 times the magnitude of its left element plus 2 times the magnitude of its right element.
    The magnitude of a regular number is just that number.

    For example, the magnitude of [9,1] is 3*9 + 2*1 = 29; the magnitude of [1,9] is 3*1 + 2*9 = 21. Magnitude
    calculations are recursive: the magnitude of [[9,1],[1,9]] is 3*29 + 2*21 = 129.

    Here are a few more magnitude examples:

    [[1,2],[[3,4],5]] becomes 143.
    [[[[0,7],4],[[7,8],[6,0]]],[8,1]] becomes 1384.
    [[[[1,1],[2,2]],[3,3]],[4,4]] becomes 445.
    [[[[3,0],[5,3]],[4,4]],[5,5]] becomes 791.
    [[[[5,0],[7,4]],[5,5]],[6,6]] becomes 1137.
    [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] becomes 3488.
    So, given this example homework assignment:

    [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
    [[[5,[2,8]],4],[5,[[9,9],0]]]
    [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
    [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
    [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
    [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
    [[[[5,4],[7,7]],8],[[8,3],8]]
    [[9,3],[[9,9],[6,[4,9]]]]
    [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
    [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
    The final sum is:

    [[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]
    The magnitude of this final sum is 4140.

    Add up all of the snailfish numbers from the homework assignment in the order they appear. What is the magnitude of
    the final sum?

    Your puzzle answer was 4457.
    """
    acc = _create_node_from_data(eval(all_lines[0]), parent_node=None, level=0)
    for line in all_lines[1:]:
        sum_str = "[" + repr(acc) + "," + line + "]"
        acc_tree = _create_node_from_data(eval(sum_str), parent_node=None, level=0)
        acc = _reduce(acc_tree)
    logging.info(f"p2 res = {_calculate_magnitude(acc)}.")


def _calculate_magnitude(node: Node) -> int:
    if node.is_leaf_node():
        return node.value
    elif node.left_and_right_nodes_have_values():
        return 3 * node.left.value + 2 * node.right.value
    else:
        return 3 * _calculate_magnitude(node.left) + 2 * _calculate_magnitude(node.right)


def _part2(all_lines: List[str]) -> None:
    """
    --- Part Two ---
    You notice a second question on the back of the homework assignment:

    What is the largest magnitude you can get from adding only two of the snailfish numbers?

    Note that snailfish addition is not commutative - that is, x + y and y + x can produce different results.

    Again considering the last example homework assignment above:

    [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
    [[[5,[2,8]],4],[5,[[9,9],0]]]
    [6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
    [[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
    [[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
    [[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
    [[[[5,4],[7,7]],8],[[8,3],8]]
    [[9,3],[[9,9],[6,[4,9]]]]
    [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
    [[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]
    The largest magnitude of the sum of any two snailfish numbers in this list is 3993. This is the magnitude of
    [[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]] + [[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]], which reduces to
    [[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]].

    What is the largest magnitude of any sum of two different snailfish numbers from the homework assignment?

    Your puzzle answer was 4784.
    """
    all_sum = []
    for l1 in all_lines:
        for l2 in all_lines:
            if l2 != l1:
                all_sum.append(_calculate_line_sum(l1, l2))
    logging.info(f"p2 res = {max(all_sum)}.")


def _calculate_line_sum(l1: str, l2: str) -> int:
    total_str = "[" + l1 + "," + l2 + "]"
    node = _create_node_from_data(eval(total_str), parent_node=None, level=0)
    node = _reduce(node)
    return _calculate_magnitude(node)
