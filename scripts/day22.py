"""Solution to day 22"""
from pathlib import Path
from typing import List
from typing import Optional
from typing import Tuple

import numpy as np
from absl import logging

from scripts._util import time_it

_CuboidSpan = Tuple[int, int]


class Cube:
    def __init__(
        self, span_x: Optional[_CuboidSpan], span_y: Optional[_CuboidSpan], span_z: Optional[_CuboidSpan], status: str
    ):
        self.span_x = span_x
        self.span_y = span_y
        self.span_z = span_z
        self.status = status

    @property
    def area(self) -> int:
        "Returns an area of the cube."
        if self.span_x is None or self.span_y is None or self.span_z is None:
            return 0

        return (
            (self.span_x[1] - self.span_x[0] + 1)
            * (self.span_y[1] - self.span_y[0] + 1)
            * (self.span_z[1] - self.span_z[0] + 1)
        )

    @staticmethod
    def combine_cube_intersection(cube1: "Cube", cube2: "Cube") -> "Cube":
        """Combine the intersection between the two cubes and return a new cube for the intersection volume.

        The status of the combined cube is the status of cube2!.
        """
        span_x = Cube._span_intersection(cube1.span_x, cube2.span_x)
        span_y = Cube._span_intersection(cube1.span_y, cube2.span_y)
        span_z = Cube._span_intersection(cube1.span_z, cube2.span_z)
        if span_x is not None and span_y is not None and span_z is not None:
            return Cube(span_x=span_x, span_y=span_y, span_z=span_z, status=cube2.status)
        else:
            return Cube(span_x=None, span_y=None, span_z=None, status=cube2.status)

    @staticmethod
    def _span_intersection(x: Optional[_CuboidSpan], y: Optional[_CuboidSpan]) -> Optional[_CuboidSpan]:
        if x is None or y is None:
            return None

        min_x, max_x = min(x), max(x)
        min_y, max_y = min(y), max(y)

        if min_y < min_x:
            if max_y < min_x:
                return None
            elif max_y <= max_x:
                return min_x, max_y
            else:
                return x

        elif min_y <= max_x:
            if max_y <= max_x:
                return y
            else:
                return min_y, max_x

        else:
            return None

    @staticmethod
    def _span_len(span: Optional[_CuboidSpan]) -> int:
        if span is None:
            return 0
        else:
            return span[1] - span[0] + 1

    def __repr__(self):
        return f"Cube(span_x={self.span_x}, span_y={self.span_y}, span_z={self.span_z}, status={self.status})"

    def is_none_cube(self):
        return True if self.span_x is None or self.span_y is None or self.span_z is None else False


def day22() -> None:
    day22_input_file_name = Path(__file__).parent.parent / "data" / "day22_input.txt"
    with open(day22_input_file_name, "r") as file:
        data = [_process_line(l.strip()) for l in file.readlines()]

    with time_it("part 1") as _:
        _part1(data)

    with time_it("part 2") as _:
        _part2(data)


def _process_line(l: str) -> Tuple[str, List[_CuboidSpan]]:
    status, data = l.split(" ")
    spans = [tuple(map(int, p.split("=")[1].split(".."))) for p in data.split(",")]
    return status, spans


def _part1(data: List[Tuple[str, List[_CuboidSpan]]]) -> None:
    """
    --- Day 22: Reactor Reboot ---
    Operating at these extreme ocean depths has overloaded the submarine's reactor; it needs to be rebooted.

    The reactor core is made up of a large 3-dimensional grid made up entirely of cubes, one cube per integer 3-
    dimensional coordinate (x,y,z). Each cube can be either on or off; at the start of the reboot process, they are all
    off. (Could it be an old model of a reactor you've seen before?)

    To reboot the reactor, you just need to set all of the cubes to either on or off by following a list of reboot steps
    (your puzzle input). Each step specifies a cuboid (the set of all cubes that have coordinates which fall within
    ranges for x, y, and z) and whether to turn all of the cubes in that cuboid on or off.

    For example, given these reboot steps:

    on x=10..12,y=10..12,z=10..12
    on x=11..13,y=11..13,z=11..13
    off x=9..11,y=9..11,z=9..11
    on x=10..10,y=10..10,z=10..10
    The first step (on x=10..12,y=10..12,z=10..12) turns on a 3x3x3 cuboid consisting of 27 cubes:

    10,10,10
    10,10,11
    10,10,12
    10,11,10
    10,11,11
    10,11,12
    10,12,10
    10,12,11
    10,12,12
    11,10,10
    11,10,11
    11,10,12
    11,11,10
    11,11,11
    11,11,12
    11,12,10
    11,12,11
    11,12,12
    12,10,10
    12,10,11
    12,10,12
    12,11,10
    12,11,11
    12,11,12
    12,12,10
    12,12,11
    12,12,12
    The second step (on x=11..13,y=11..13,z=11..13) turns on a 3x3x3 cuboid that overlaps with the first. As a result, only 19 additional cubes turn on; the rest are already on from the previous step:

    11,11,13
    11,12,13
    11,13,11
    11,13,12
    11,13,13
    12,11,13
    12,12,13
    12,13,11
    12,13,12
    12,13,13
    13,11,11
    13,11,12
    13,11,13
    13,12,11
    13,12,12
    13,12,13
    13,13,11
    13,13,12
    13,13,13
    The third step (off x=9..11,y=9..11,z=9..11) turns off a 3x3x3 cuboid that overlaps partially with some cubes that are on, ultimately turning off 8 cubes:

    10,10,10
    10,10,11
    10,11,10
    10,11,11
    11,10,10
    11,10,11
    11,11,10
    11,11,11
    The final step (on x=10..10,y=10..10,z=10..10) turns on a single cube, 10,10,10. After this last step, 39 cubes are on.

    The initialization procedure only uses cubes that have x, y, and z positions of at least -50 and at most 50. For now, ignore cubes outside this region.

    Here is a larger example:

    on x=-20..26,y=-36..17,z=-47..7
    on x=-20..33,y=-21..23,z=-26..28
    on x=-22..28,y=-29..23,z=-38..16
    on x=-46..7,y=-6..46,z=-50..-1
    on x=-49..1,y=-3..46,z=-24..28
    on x=2..47,y=-22..22,z=-23..27
    on x=-27..23,y=-28..26,z=-21..29
    on x=-39..5,y=-6..47,z=-3..44
    on x=-30..21,y=-8..43,z=-13..34
    on x=-22..26,y=-27..20,z=-29..19
    off x=-48..-32,y=26..41,z=-47..-37
    on x=-12..35,y=6..50,z=-50..-2
    off x=-48..-32,y=-32..-16,z=-15..-5
    on x=-18..26,y=-33..15,z=-7..46
    off x=-40..-22,y=-38..-28,z=23..41
    on x=-16..35,y=-41..10,z=-47..6
    off x=-32..-23,y=11..30,z=-14..3
    on x=-49..-5,y=-3..45,z=-29..18
    off x=18..30,y=-20..-8,z=-3..13
    on x=-41..9,y=-7..43,z=-33..15
    on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
    on x=967..23432,y=45373..81175,z=27513..53682
    The last two steps are fully outside the initialization procedure area; all other steps are fully within it. After executing these steps in the initialization procedure region, 590784 cubes are on.

    Execute the reboot steps. Afterward, considering only cubes in the region x=-50..50,y=-50..50,z=-50..50, how many cubes are on?

    Your puzzle answer was 587097.


    """
    cuboid_span = (-50, 51)
    side_dim = cuboid_span[1] - cuboid_span[0]
    all_cubes = np.zeros((side_dim, side_dim, side_dim))
    for status, spans in data:
        slices = _slice_from_span(spans, cuboid_span)
        if slices is not None:
            slice_x, slice_y, slice_z = slices
        else:
            continue

        if status == "on":
            all_cubes[slice_x, slice_y, slice_z] = 1
        else:
            all_cubes[slice_x, slice_y, slice_z] = 0

    logging.info(f"p1 res = {np.sum(all_cubes)}")


def _slice_from_span(spans: List[_CuboidSpan], cuboid_span: _CuboidSpan) -> Optional[Tuple[slice, slice, slice]]:
    out_slices = []
    for span in spans:
        if not (cuboid_span[0] <= span[0] and span[1] + 1 <= cuboid_span[1]):
            return None
        else:
            out_slices.append(slice(span[0] - cuboid_span[0], span[1] + 1 - cuboid_span[0]))
    return out_slices


def _part2(data: List[Tuple[str, List[_CuboidSpan]]]) -> None:
    """
    --- Part Two ---
    Now that the initialization procedure is complete, you can reboot the reactor.

    Starting with all cubes off, run all of the reboot steps for all cubes in the reactor.

    Consider the following reboot steps:

    on x=-5..47,y=-31..22,z=-19..33
    on x=-44..5,y=-27..21,z=-14..35
    on x=-49..-1,y=-11..42,z=-10..38
    on x=-20..34,y=-40..6,z=-44..1
    off x=26..39,y=40..50,z=-2..11
    on x=-41..5,y=-41..6,z=-36..8
    off x=-43..-33,y=-45..-28,z=7..25
    on x=-33..15,y=-32..19,z=-34..11
    off x=35..47,y=-46..-34,z=-11..5
    on x=-14..36,y=-6..44,z=-16..29
    on x=-57795..-6158,y=29564..72030,z=20435..90618
    on x=36731..105352,y=-21140..28532,z=16094..90401
    on x=30999..107136,y=-53464..15513,z=8553..71215
    on x=13528..83982,y=-99403..-27377,z=-24141..23996
    on x=-72682..-12347,y=18159..111354,z=7391..80950
    on x=-1060..80757,y=-65301..-20884,z=-103788..-16709
    on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856
    on x=-52752..22273,y=-49450..9096,z=54442..119054
    on x=-29982..40483,y=-108474..-28371,z=-24328..38471
    on x=-4958..62750,y=40422..118853,z=-7672..65583
    on x=55694..108686,y=-43367..46958,z=-26781..48729
    on x=-98497..-18186,y=-63569..3412,z=1232..88485
    on x=-726..56291,y=-62629..13224,z=18033..85226
    on x=-110886..-34664,y=-81338..-8658,z=8914..63723
    on x=-55829..24974,y=-16897..54165,z=-121762..-28058
    on x=-65152..-11147,y=22489..91432,z=-58782..1780
    on x=-120100..-32970,y=-46592..27473,z=-11695..61039
    on x=-18631..37533,y=-124565..-50804,z=-35667..28308
    on x=-57817..18248,y=49321..117703,z=5745..55881
    on x=14781..98692,y=-1341..70827,z=15753..70151
    on x=-34419..55919,y=-19626..40991,z=39015..114138
    on x=-60785..11593,y=-56135..2999,z=-95368..-26915
    on x=-32178..58085,y=17647..101866,z=-91405..-8878
    on x=-53655..12091,y=50097..105568,z=-75335..-4862
    on x=-111166..-40997,y=-71714..2688,z=5609..50954
    on x=-16602..70118,y=-98693..-44401,z=5197..76897
    on x=16383..101554,y=4615..83635,z=-44907..18747
    off x=-95822..-15171,y=-19987..48940,z=10804..104439
    on x=-89813..-14614,y=16069..88491,z=-3297..45228
    on x=41075..99376,y=-20427..49978,z=-52012..13762
    on x=-21330..50085,y=-17944..62733,z=-112280..-30197
    on x=-16478..35915,y=36008..118594,z=-7885..47086
    off x=-98156..-27851,y=-49952..43171,z=-99005..-8456
    off x=2032..69770,y=-71013..4824,z=7471..94418
    on x=43670..120875,y=-42068..12382,z=-24787..38892
    off x=37514..111226,y=-45862..25743,z=-16714..54663
    off x=25699..97951,y=-30668..59918,z=-15349..69697
    off x=-44271..17935,y=-9516..60759,z=49131..112598
    on x=-61695..-5813,y=40978..94975,z=8655..80240
    off x=-101086..-9439,y=-7088..67543,z=33935..83858
    off x=18020..114017,y=-48931..32606,z=21474..89843
    off x=-77139..10506,y=-89994..-18797,z=-80..59318
    off x=8476..79288,y=-75520..11602,z=-96624..-24783
    on x=-47488..-1262,y=24338..100707,z=16292..72967
    off x=-84341..13987,y=2429..92914,z=-90671..-1318
    off x=-37810..49457,y=-71013..-7894,z=-105357..-13188
    off x=-27365..46395,y=31009..98017,z=15428..76570
    off x=-70369..-16548,y=22648..78696,z=-1892..86821
    on x=-53470..21291,y=-120233..-33476,z=-44150..38147
    off x=-93533..-4276,y=-16170..68771,z=-104985..-24507
    After running the above reboot steps, 2758514936282235 cubes are on. (Just for fun, 474140 of those are also in the
    initialization procedure region.)

    Starting again with all cubes off, execute all reboot steps. Afterward, considering all cubes, how many cubes are
    on?

    Your puzzle answer was 1359673068597669
    """
    all_cubes = [
        Cube(span_x=span_x, span_y=span_y, span_z=span_z, status=cmd) for cmd, (span_x, span_y, span_z) in data
    ]
    logging.info(f"p2 res = {_count_ones(all_cubes)}")


def _count_ones(cubes: List[Cube]) -> int:
    """Returns the number of ones after combining all cubes."""
    if len(cubes) == 0:
        return 0
    elif len(cubes) == 1:
        if cubes[0].status == "on":
            return cubes[0].area
        else:
            # Negative status does not contribute to the one count.
            return 0
    else:
        last_cube = cubes[-1]
        intersection_cubes = []
        for x in cubes[:-1]:
            temp = Cube.combine_cube_intersection(cube1=last_cube, cube2=x)
            if not temp.is_none_cube():
                intersection_cubes.append(temp)

        if last_cube.status == "on":
            return _count_ones(cubes[:-1]) + last_cube.area - _count_ones(intersection_cubes)
        else:
            return _count_ones(cubes[:-1]) - _count_ones(intersection_cubes)
