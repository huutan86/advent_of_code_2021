# Source code to run the solution for all days.
import importlib
from pathlib import Path

from absl import app
from absl import flags
from absl import logging

flags.DEFINE_integer("day", None, "The 1-based index of the day to solve the challenge.")
flags.mark_flag_as_required("day")
FLAGS = flags.FLAGS


def _import_modules_of_all_days():
    for day in range(1, 26):
        module_name = f"day{day}"
        if Path(f"./{module_name}.py").exists():
            globals()[module_name] = getattr(importlib.import_module(module_name), module_name)
            logging.info(f"Imported day {day} module.")


def main(argv) -> None:
    del argv
    logging.info(f"Solving challenge of day {FLAGS.day}")

    try:
        globals()[f"day{FLAGS.day}"]()
    except Exception as e:
        raise ValueError(f"Can't call the function on day {FLAGS.day}")
        raise e


_import_modules_of_all_days()
if __name__ == "__main__":
    app.run(main)
