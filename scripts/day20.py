"""Solution to day 19"""
from itertools import product
from pathlib import Path
from typing import Dict
from typing import List
from typing import Tuple

import numpy as np
from absl import logging

from scripts._util import time_it


def day20() -> None:
    day20_input_file_name = Path(__file__).parent.parent / "data" / "day20_input.txt"
    with open(day20_input_file_name, "r") as file:
        all_lines = [l.strip() for l in file.readlines()]

    algo, all_ones_pixels = _parse_data(all_lines)
    with time_it("part 1") as _:
        _part1(algo, all_ones_pixels)

    with time_it("part 2") as _:
        _part2(algo, all_ones_pixels)


def _parse_data(all_lines: List[str]) -> Tuple[Dict[int, int], List[Tuple[int, ...]]]:
    num_lines = len(all_lines)
    line_idx = 0
    algo = ""
    while all_lines[line_idx] != "":
        algo += all_lines[line_idx]
        line_idx += 1
    algo = {k: 1 if c == "#" else 0 for k, c in enumerate(algo)}

    line_idx += 1
    num_cols = len(all_lines[line_idx])
    all_one_pixels = [
        (r - line_idx, c) for r in range(line_idx, num_lines) for c in range(num_cols) if all_lines[r][c] == "#"
    ]
    return algo, all_one_pixels


def _part1(algo: Dict[int, int], all_one_pixels: List[Tuple[int, ...]]) -> None:
    """
    --- Day 20: Trench Map ---
    With the scanners fully deployed, you turn their attention to mapping the floor of the ocean trench.

    When you get back the image from the scanners, it seems to just be random noise. Perhaps you can combine an image
    enhancement algorithm and the input image (your puzzle input) to clean it up a little.

    For example:

    ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
    #..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
    .######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
    .#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
    .#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
    ...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
    ..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

    #..#.
    #....
    ##..#
    ..#..
    ..###
    The first section is the image enhancement algorithm. It is normally given on a single line, but it has been wrapped
    to multiple lines in this example for legibility. The second section is the input image, a two-dimensional grid of
    light pixels (#) and dark pixels (.).

    The image enhancement algorithm describes how to enhance an image by simultaneously converting all pixels in the
    input image into an output image. Each pixel of the output image is determined by looking at a 3x3 square of pixels
    centered on the corresponding input image pixel. So, to determine the value of the pixel at (5,10) in the output
    image, nine pixels from the input image need to be considered: (4,9), (4,10), (4,11), (5,9), (5,10), (5,11), (6,9),
    (6,10), and (6,11). These nine input pixels are combined into a single binary number that is used as an index in the
    image enhancement algorithm string.

    For example, to determine the output pixel that corresponds to the very middle pixel of the input image, the nine
    pixels marked by [...] would need to be considered:

    # . . # .
    #[. . .].
    #[# . .]#
    .[. # .].
    . . # # #
    Starting from the top-left and reading across each row, these pixels are ..., then #.., then .#.; combining these
    forms ...#...#.. By turning dark pixels (.) into 0 and light pixels (#) into 1, the binary number 000100010 can be
    formed, which is 34 in decimal.

    The image enhancement algorithm string is exactly 512 characters long, enough to match every possible 9-bit binary
    number. The first few characters of the string (numbered starting from zero) are as follows:

    0         10        20        30  34    40        50        60        70
    |         |         |         |   |     |         |         |         |
    ..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
    In the middle of this first group of characters, the character at index 34 can be found: #. So, the output pixel in
    the center of the output image should be #, a light pixel.

    This process can then be repeated to calculate every pixel of the output image.

    Through advances in imaging technology, the images being operated on here are infinite in size. Every pixel of the
    infinite output image needs to be calculated exactly based on the relevant pixels of the input image. The small
    input image you have is only a small region of the actual infinite input image; the rest of the input image consists
    of dark pixels (.). For the purposes of the example, to save on space, only a portion of the infinite-sized input and
    output images will be shown.

    The starting input image, therefore, looks something like this, with more dark pixels (.) extending forever in every
    direction not shown here:

    ...............
    ...............
    ...............
    ...............
    ...............
    .....#..#......
    .....#.........
    .....##..#.....
    .......#.......
    .......###.....
    ...............
    ...............
    ...............
    ...............
    ...............
    By applying the image enhancement algorithm to every pixel simultaneously, the following output image can be
    obtained:

    ...............
    ...............
    ...............
    ...............
    .....##.##.....
    ....#..#.#.....
    ....##.#..#....
    ....####..#....
    .....#..##.....
    ......##..#....
    .......#.#.....
    ...............
    ...............
    ...............
    ...............
    Through further advances in imaging technology, the above output image can also be used as an input image! This
    allows it to be enhanced a second time:

    ...............
    ...............
    ...............
    ..........#....
    ....#..#.#.....
    ...#.#...###...
    ...#...##.#....
    ...#.....#.#...
    ....#.#####....
    .....#.#####...
    ......##.##....
    .......###.....
    ...............
    ...............
    ...............
    Truly incredible - now the small details are really starting to come through. After enhancing the original input
    image twice, 35 pixels are lit.

    Start with the original input image and apply the image enhancement algorithm twice, being careful to account for
    the infinite size of the images. How many pixels are lit in the resulting image?

    Your puzzle answer was 5503.
    """
    res = _enhance_and_count_lit(algo, all_one_pixels, num_iter=2)
    logging.info(f"p1 res = {res}.")


def _enhance_and_count_lit(algo: Dict[int, int], all_one_pixels: List[Tuple[int, ...]], num_iter: int) -> int:
    all_one_pixels = all_one_pixels.copy()
    if algo[0] == 0 and algo[511] == 1:
        value_flip = False
    elif algo[0] == 1 and algo[511] == 0:
        value_flip = True
    else:
        raise ValueError("Unusable!")

    all_ones = all_one_pixels
    all_zeros = []

    for iter in range(num_iter):
        new_all_ones, new_all_zeros = [], []
        finite_num_element_list = all_ones if len(all_ones) > 0 else all_zeros
        max_r, min_r, max_c, min_c = _min_max_rows_and_cols(finite_num_element_list)

        for r, c in product(range(min_r - 1, max_r + 2), range(min_c - 1, max_c + 2)):
            p_neighbors = [(r + dr, c + dc) for dr, dc in product([-1, 0, 1], [-1, 0, 1])]

            if not value_flip == 1:
                vect = "".join("1" if n in all_ones else "0" for n in p_neighbors)
                val = int(vect, 2)
                if algo[val] == 1:
                    new_all_ones.append((r, c))
            else:
                if len(all_zeros) == 0 and len(all_ones) > 0:
                    # Finite number of 1's.
                    vect = "".join("1" if n in all_ones else "0" for n in p_neighbors)
                    val = int(vect, 2)
                    if algo[val] == 0:
                        new_all_zeros.append((r, c))
                elif len(all_ones) == 0 and len(all_zeros) > 0:
                    # Finite number of 0's.
                    vect = "".join("0" if n in all_zeros else "1" for n in p_neighbors)
                    val = int(vect, 2)
                    if algo[val] == 1:
                        new_all_ones.append((r, c))
                else:
                    raise RuntimeError("Unhandled case!")
        all_ones, all_zeros = new_all_ones, new_all_zeros
        logging.info(f"Iter = {iter}, one_count = {len(all_ones) if len(all_ones) > 0 else np.inf}")
    return len(all_ones)


def _min_max_rows_and_cols(all_one_pixels: List[Tuple[int, ...]]) -> Tuple[int, ...]:
    return (
        max(p[0] for p in all_one_pixels),
        min(p[0] for p in all_one_pixels),
        max(p[1] for p in all_one_pixels),
        min(p[1] for p in all_one_pixels),
    )


def _part2(algo: Dict[int, int], all_one_pixels: List[Tuple[int, ...]]) -> None:
    """
    --- Part Two ---
    You still can't quite make out the details in the image. Maybe you just didn't enhance it enough.

    If you enhance the starting input image in the above example a total of 50 times, 3351 pixels are lit in the final
    output image.

    Start again with the original input image and apply the image enhancement algorithm 50 times. How many pixels are
    lit in the resulting image?

    Your puzzle answer was 19156.
    """
    # TODO: investigate how to speed this up. The runtime was 2000s. Ok for a Mitch's drop off to preschool!!!
    res = _enhance_and_count_lit(algo, all_one_pixels, num_iter=50)
    logging.info(f"p2 res = {res}.")
