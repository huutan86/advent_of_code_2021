"""Source code to solve day 9 challenge."""
from pathlib import Path
from typing import List
from typing import Tuple

import numpy as np
from absl import logging
from scipy.ndimage import shift
from skimage.measure import label
from skimage.measure import regionprops

from scripts._util import time_it


def process_line(line: str) -> List[int]:
    return [int(x) for x in list(line)]


def day9() -> None:
    day9_input_file_name = Path(__file__).parent.parent / "data" / "day9_input.txt"
    with open(day9_input_file_name, "r") as file:
        height_map = np.array([process_line(l.strip()) for l in file.readlines()])

    with time_it("part 1") as _:
        _part1(height_map)

    with time_it("part 2") as _:
        _part2(height_map)


def _shift_image(in_im: np.ndarray, shift_amt: Tuple[int, int]) -> np.ndarray:
    return shift(in_im, shift=shift_amt, mode="constant", cval=10)


def _part1(height_map: np.ndarray) -> None:
    """
    --- Day 9: Smoke Basin ---
    These caves seem to be lava tubes. Parts are even still volcanically active; small hydrothermal vents release smoke
    into the caves that slowly settles like rain.

    If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer. The
    submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).

    Smoke flows to the lowest point of the area it's in. For example, consider the following heightmap:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    Each number corresponds to the height of a particular location, where 9 is the highest and 0 is the lowest a
    location can be.

    Your first goal is to find the low points - the locations that are lower than any of its adjacent locations. Most
    locations have four adjacent locations (up, down, left, and right); locations on the edge or corner of the map have
    three or two adjacent locations, respectively. (Diagonal locations do not count as adjacent.)

    In the above example, there are four low points, all highlighted: two are in the first row (a 1 and a 0), one is in
    the third row (a 5), and one is in the bottom row (also a 5). All other locations on the heightmap have some lower
    adjacent location, and so are not low points.

    The risk level of a low point is 1 plus its height. In the above example, the risk levels of the low points are 2,
    1, 6, and 6. The sum of the risk levels of all low points in the heightmap is therefore 15.

    Find all of the low points on your heightmap. What is the sum of the risk levels of all low points on your heightmap?

    Your puzzle answer was 439.


    """
    im_1 = _shift_image(height_map, shift_amt=(0, 1)) - height_map
    im_2 = _shift_image(height_map, shift_amt=(0, -1)) - height_map
    im_3 = _shift_image(height_map, shift_amt=(1, 0)) - height_map
    im_4 = _shift_image(height_map, shift_amt=(-1, 0)) - height_map
    res1 = np.logical_and(im_1 > 0, im_2 > 0)
    res2 = np.logical_and(im_3 > 0, im_4 > 0)
    res5 = np.logical_and(res1, res2)
    logging.info(f"p1 res = {np.sum(x + 1 for x in height_map[res5])}")


def _part2(height_map: np.ndarray) -> None:
    """
    --- Part Two ---
    Next, you need to find the largest basins so you know what areas are most important to avoid.

    A basin is all locations that eventually flow downward to a single low point. Therefore, every low point has a basin
    , although some basins are very small. Locations of height 9 do not count as being in any basin, and all other
    locations will always be part of exactly one basin.

    The size of a basin is the number of locations within the basin, including the low point. The example above has four
    basins.

    The top-left basin, size 3:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    The top-right basin, size 9:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    The middle basin, size 14:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    The bottom-right basin, size 9:

    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    Find the three largest basins and multiply their sizes together. In the above example, this is 9 * 14 * 9 = 1134.

    What do you get if you multiply together the sizes of the three largest basins?

    Your puzzle answer was 900900.
    """
    non_nine_image = height_map != 9
    label_img = label(non_nine_image, connectivity=1)
    regions = regionprops(label_img)
    region_areas = sorted([x.area for x in regions], reverse=True)[:3]
    logging.info(f"p2 res = {region_areas[0] * region_areas[1] * region_areas[2]}")
