"""Source code to solve day 5 challenge."""
from itertools import permutations
from pathlib import Path
from typing import Dict
from typing import List
from typing import Tuple

from absl import logging

from scripts._util import time_it

_SEGMENT_BY_NUMBERS = {
    0: set(["a", "b", "c", "e", "f", "g"]),
    1: set(["c", "f"]),
    2: set(["a", "c", "d", "e", "g"]),
    3: set(["a", "c", "d", "f", "g"]),
    4: set(["b", "c", "d", "f"]),
    5: set(["a", "b", "d", "f", "g"]),
    6: set(["a", "b", "d", "e", "f", "g"]),
    7: set(["a", "c", "f"]),
    8: set(["a", "b", "c", "d", "e", "f", "g"]),
    9: set(["a", "b", "c", "d", "f", "g"]),
}


def process_input(x: str) -> Tuple[List[str], List[str]]:
    p1, p2 = x.split("|")
    p1 = p1.strip().split(" ")
    p2 = p2.strip().split(" ")
    return p1, p2


def day8() -> None:
    day8_input_file_name = Path(__file__).parent.parent / "data" / "day8_input.txt"
    with open(day8_input_file_name, "r") as file:
        data = [process_input(l.strip()) for l in file.readlines()]

    with time_it("part 1") as _:
        _part1(data)

    with time_it("part 2") as _:
        _part2(data)


def _part1(data: List[Tuple[List[str], List[str]]]) -> None:
    """
    --- Day 8: Seven Segment Search ---
    You barely reach the safety of the cave when the whale smashes into the cave mouth, collapsing it. Sensors indicate
    another exit to this cave at a much greater depth, so you have no choice but to press on.

    As your submarine slowly makes its way through the cave system, you notice that the four-digit seven-segment
    displays in your submarine are malfunctioning; they must have been damaged during the escape. You'll be in a lot of
    trouble without them, so you'd better figure out what's wrong.

    Each digit of a seven-segment display is rendered by turning on or off any of seven segments named a through g:

      0:      1:      2:      3:      4:
     aaaa    ....    aaaa    aaaa    ....
    b    c  .    c  .    c  .    c  b    c
    b    c  .    c  .    c  .    c  b    c
     ....    ....    dddd    dddd    dddd
    e    f  .    f  e    .  .    f  .    f
    e    f  .    f  e    .  .    f  .    f
     gggg    ....    gggg    gggg    ....

      5:      6:      7:      8:      9:
     aaaa    aaaa    aaaa    aaaa    aaaa
    b    .  b    .  .    c  b    c  b    c
    b    .  b    .  .    c  b    c  b    c
     dddd    dddd    ....    dddd    dddd
    .    f  e    f  .    f  e    f  .    f
    .    f  e    f  .    f  e    f  .    f
     gggg    gggg    ....    gggg    gggg
    So, to render a 1, only segments c and f would be turned on; the rest would be off. To render a 7, only segments a,
    c, and f would be turned on.

    The problem is that the signals which control the segments have been mixed up on each display. The submarine is
    still trying to display numbers by producing output on signal wires a through g, but those wires are connected to
    segments randomly. Worse, the wire/segment connections are mixed up separately for each four-digit display! (All of
    the digits within a display use the same connections, though.)

    So, you might know that only signal wires b and g are turned on, but that doesn't mean segments b and g are turned
    on: the only digit that uses two segments is 1, so it must mean segments c and f are meant to be on. With just that
    information, you still can't tell which wire (b/g) goes to which segment (c/f). For that, you'll need to collect
    more information.

    For each display, you watch the changing signals for a while, make a note of all ten unique signal patterns you see,
    and then write down a single four digit output value (your puzzle input). Using the signal patterns, you should be
    able to work out which pattern corresponds to which digit.

    For example, here is what you might see in a single entry in your notes:

    acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
    cdfeb fcadb cdfeb cdbaf
    (The entry is wrapped here to two lines so it fits; in your notes, it will all be on a single line.)

    Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output value. Within an
    entry, the same wire/segment connections are used (but you don't know what the connections actually are). The unique
    signal patterns correspond to the ten different ways the submarine tries to render a digit using the current
    wire/segment connections. Because 7 is the only digit that uses three segments, dab in the above example means that
    to render a 7, signal lines d, a, and b are on. Because 4 is the only digit that uses four segments, eafb means that
    to render a 4, signal lines e, a, f, and b are on.

    Using this information, you should be able to work out which combination of signal wires corresponds to each of the
    ten digits. Then, you can decode the four digit output value. Unfortunately, in the above example, all of the digits
     in the output value (cdfeb fcadb cdfeb cdbaf) use five segments and are more difficult to deduce.

    For now, focus on the easy digits. Consider this larger example:

    be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
    fdgacbe cefdb cefbgd gcbe
    edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
    fcgedb cgb dgebacf gc
    fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
    cg cg fdcagb cbg
    fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
    efabcd cedba gadfec cb
    aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
    gecf egdcabf bgf bfgea
    fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
    gebdcfa ecba ca fadegcb
    dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
    cefg dcbef fcge gbcadfe
    bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
    ed bcgafe cdgba cbgef
    egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
    gbdfcae bgc cg cgb
    gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
    fgae cfgab fg bagce
    Because the digits 1, 4, 7, and 8 each use a unique number of segments, you should be able to tell which
    combinations of signals correspond to those digits. Counting only digits in the output values (the part after | on
    each line), in the above example, there are 26 instances of digits that use a unique number of segments (highlighted above).

    In the output values, how many times do digits 1, 4, 7, or 8 appear?

    Your puzzle answer was 365.
    """
    all_p2s = [x[1] for x in data]
    all_p2_lens = [len(x) for y in all_p2s for x in y]
    logging.info(f"p1 res {all_p2_lens.count(2) + all_p2_lens.count(3) + all_p2_lens.count(4) + all_p2_lens.count(7)}")


def _find_mapping(p1: List[str]) -> Dict[str, str]:
    # [Legacy version] Hand-rule to find the mapping.
    all_segments = {"a", "b", "c", "d", "e", "f", "g"}
    out_map = {}
    one_str = next(x for x in p1 if len(x) == 2)
    seven_str = next(x for x in p1 if len(x) == 3)
    out_map["a"] = (set(list(seven_str)) - set(list(one_str))).pop()

    # 'f' is the segments that appears in 9 candidates
    segment_count_per_candidates = {x: 0 for x in all_segments}
    for seg in all_segments:
        for c in p1:
            if seg in c:
                segment_count_per_candidates[seg] += 1
    out_map["f"] = next(k for k, v in segment_count_per_candidates.items() if v == 9)

    out_map["c"] = (set(list(one_str)) - set(out_map["f"])).pop()

    four_str = next(x for x in p1 if len(x) == 4)
    all_seg_in_one_four_seven = set(list(one_str)) | set(list(four_str)) | set(list(seven_str))
    all_len_six_candidates = [x for x in p1 if len(x) == 6]
    for c in all_len_six_candidates:
        if all_seg_in_one_four_seven.issubset(set(c)):
            out_map["g"] = (set(c) - all_seg_in_one_four_seven).pop()

    all_len_five_candidates = [x for x in p1 if len(x) == 5]
    all_chars_for_a_c_f_g = set(out_map.values())
    for c in all_len_five_candidates:
        if all_chars_for_a_c_f_g.issubset(set(c)):
            out_map["d"] = (set(c) - all_chars_for_a_c_f_g).pop()

    out_map["b"] = (set(four_str) - set([out_map["c"], out_map["f"], out_map["d"]])).pop()
    out_map["e"] = (all_segments - set(out_map.values())).pop()
    return out_map


def _find_mapping2(p1: List[str]) -> Dict[str, str]:
    """Brute force search over all permutations."""
    required_set = set("".join(sorted(x)) for x in p1)
    for x in permutations("abcdefg"):
        mapping_candidate = {k: v for k, v in zip("abcdefg", x)}
        gen = {"".join(sorted(map(mapping_candidate.get, segment_set))) for segment_set in _SEGMENT_BY_NUMBERS.values()}
        if gen == required_set:
            return mapping_candidate


def _decode_value(p2, mapping: Dict[str, str]) -> int:
    segment_set_by_numbers_mapped = {k: set(mapping[x] for x in v) for k, v in _SEGMENT_BY_NUMBERS.items()}
    return int("".join(str(k) for item in p2 for k, v in segment_set_by_numbers_mapped.items() if set(item) == v))


def _part2(data: List[Tuple[List[str], List[str]]]) -> None:
    """
    --- Part Two ---
    Through a little deduction, you should now be able to determine the remaining digits. Consider again the first
    example above:

    acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
    cdfeb fcadb cdfeb cdbaf
    After some careful analysis, the mapping between signal wires and segments only make sense in the following
    configuration:

     dddd
    e    a
    e    a
     ffff
    g    b
    g    b
     cccc
    So, the unique signal patterns would correspond to the following digits:

    acedgfb: 8
    cdfbe: 5
    gcdfa: 2
    fbcad: 3
    dab: 7
    cefabd: 9
    cdfgeb: 6
    eafb: 4
    cagedb: 0
    ab: 1
    Then, the four digits of the output value can be decoded:

    cdfeb: 5
    fcadb: 3
    cdfeb: 5
    cdbaf: 3
    Therefore, the output value for this entry is 5353.

    Following this same process for each entry in the second, larger example above, the output value of each entry can
    be determined:

    fdgacbe cefdb cefbgd gcbe: 8394
    fcgedb cgb dgebacf gc: 9781
    cg cg fdcagb cbg: 1197
    efabcd cedba gadfec cb: 9361
    gecf egdcabf bgf bfgea: 4873
    gebdcfa ecba ca fadegcb: 8418
    cefg dcbef fcge gbcadfe: 4548
    ed bcgafe cdgba cbgef: 1625
    gbdfcae bgc cg cgb: 8717
    fgae cfgab fg bagce: 4315
    Adding all of the output values in this larger example produces 61229.

    For each entry, determine all of the wire/segment connections and decode the four-digit output values. What do you
    get if you add up all of the output values?

    Your puzzle answer was 975706.

    # See a better sol here: https://github.com/exoji2e/aoc21/blob/main/08/day08.py
    """
    all_values = []
    for p1, p2 in data:
        mapping = _find_mapping2(p1)
        all_values.append(_decode_value(p2, mapping))
    logging.info(f"p2 res = {sum(all_values)}")
