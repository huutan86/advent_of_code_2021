"""Source code to solve day 12 challenge."""
# See a better solution using memoization here:
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
from absl import logging

from scripts._util import time_it


def _proc_line(line: str) -> List[int]:
    return tuple(int(x) for x in line.split(","))


def day13() -> None:
    day13_input_file_name = Path(__file__).parent.parent / "data" / "day13_input.txt"
    with open(day13_input_file_name, "r") as file:
        all_lines = [l.strip() for l in file.readlines()]

    dot_coords = [_proc_line(l) for l in all_lines if len(l) > 0 and l[0].isnumeric()]
    fold_instructions = [l.split(" ")[2].split("=") for l in all_lines if l.startswith("fold along")]

    all_xs = [coord[0] for coord in dot_coords]
    all_ys = [coord[1] for coord in dot_coords]
    num_x = max(all_xs) + 1
    num_y = max(all_ys) + 1
    data = np.zeros((num_y, num_x))
    for x, y in zip(all_xs, all_ys):
        data[y, x] = 1

    with time_it("part 1") as _:
        _part1(data, fold_instructions)

    with time_it("part 2") as _:
        _part2(data, fold_instructions)


def _part1(table: np.ndarray, folded_instructions: List[str]) -> None:
    """
    --- Day 13: Transparent Origami ---
    You reach another volcanically active part of the cave. It would be nice if you could do some kind of thermal
    imaging so you could tell ahead of time which caves are too hot to safely enter.

    Fortunately, the submarine seems to be equipped with a thermal camera! When you activate it, you are greeted with:

    Congratulations on your purchase! To activate this infrared thermal imaging
    camera system, please enter the code found on page 1 of the manual.
    Apparently, the Elves have never used this feature. To your surprise, you manage to find the manual; as you go to
    open it, page 1 falls out. It's a large sheet of transparent paper! The transparent paper is marked with random dots
    and includes instructions on how to fold it up (your puzzle input). For example:

    6,10
    0,14
    9,10
    0,3
    10,4
    4,11
    6,0
    6,12
    4,1
    0,13
    10,12
    3,4
    3,0
    8,4
    1,10
    2,14
    8,10
    9,0

    fold along y=7
    fold along x=5
    The first section is a list of dots on the transparent paper. 0,0 represents the top-left coordinate. The first value, x, increases to the right. The second value, y, increases downward. So, the coordinate 3,0 is to the right of 0,0, and the coordinate 0,7 is below 0,0. The coordinates in this example form the following pattern, where # is a dot on the paper and . is an empty, unmarked position:

    ...#..#..#.
    ....#......
    ...........
    #..........
    ...#....#.#
    ...........
    ...........
    ...........
    ...........
    ...........
    .#....#.##.
    ....#......
    ......#...#
    #..........
    #.#........
    Then, there is a list of fold instructions. Each instruction indicates a line on the transparent paper and wants you
    to fold the paper up (for horizontal y=... lines) or left (for vertical x=... lines). In this example, the first
    fold instruction is fold along y=7, which designates the line formed by all of the positions where y is 7 (marked
    here with -):

    ...#..#..#.
    ....#......
    ...........
    #..........
    ...#....#.#
    ...........
    ...........
    -----------
    ...........
    ...........
    .#....#.##.
    ....#......
    ......#...#
    #..........
    #.#........
    Because this is a horizontal line, fold the bottom half up. Some of the dots might end up overlapping after the fold
    is complete, but dots will never appear exactly on a fold line. The result of doing this fold looks like this:

    #.##..#..#.
    #...#......
    ......#...#
    #...#......
    .#.#..#.###
    ...........
    ...........
    Now, only 17 dots are visible.

    Notice, for example, the two dots in the bottom left corner before the transparent paper is folded; after the fold
    is complete, those dots appear in the top left corner (at 0,0 and 0,1). Because the paper is transparent, the dot
    just below them in the result (at 0,3) remains visible, as it can be seen through the transparent paper.

    Also notice that some dots can end up overlapping; in this case, the dots merge together and become a single dot.

    The second fold instruction is fold along x=5, which indicates this line:

    #.##.|#..#.
    #...#|.....
    .....|#...#
    #...#|.....
    .#.#.|#.###
    .....|.....
    .....|.....
    Because this is a vertical line, fold left:

    #####
    #...#
    #...#
    #...#
    #####
    .....
    .....
    The instructions made a square!

    The transparent paper is pretty big, so for now, focus on just completing the first fold. After the first fold in
    the example above, 17 dots are visible - dots that end up overlapping after the fold is completed count as a single
    dot.

    How many dots are visible after completing just the first fold instruction on your transparent paper?

    Your puzzle answer was 763.


    """
    first_fold_axis, first_fold_coords = folded_instructions[0][0], int(folded_instructions[0][1])
    logging.info(f"p1 res = {np.sum(_fold_once(table, first_fold_axis, first_fold_coords))}.")


def _fold_once(table: np.ndarray, fold_axis: str, fold_coord: int) -> np.ndarray:
    if fold_axis == "x":
        table_1 = table[:, :fold_coord]
        table_2 = table[:, fold_coord + 1 :]
    else:
        table_1 = table[:fold_coord, :]
        table_2 = table[fold_coord + 1 :, :]

    t1_nrows, t1_ncols = table_1.shape
    t2_nrows, t2_ncols = table_2.shape
    new_nrows, new_ncols = max([t1_nrows, t2_nrows]), max([t1_ncols, t2_ncols])
    t1_padded = np.zeros((new_nrows, new_ncols))
    t2_padded = np.zeros_like(t1_padded)
    if fold_axis == "x":
        t1_padded[:, -t1_ncols:] = table_1
        t2_padded[:, -t2_ncols:] = table_2[:, ::-1]
    else:
        t1_padded[-t1_nrows:, :] = table_1
        t2_padded[-t2_nrows:, :] = table_2[::-1, :]
    return np.clip(t1_padded + t2_padded, a_min=0, a_max=1)


def _part2(table: np.ndarray, folded_instructions: List[str]) -> None:
    """
    Finish folding the transparent paper according to the instructions. The manual says the code is always eight capital
    letters.

    What code do you use to activate the infrared thermal imaging camera system?

    Your puzzle answer was RHALRCRA.
    """
    for instr in folded_instructions:
        fold_axis, fold_coords = instr[0], int(instr[1])
        table = _fold_once(table, fold_axis, fold_coords)
    plt.figure(1)
    plt.imshow(table)
    plt.show()
