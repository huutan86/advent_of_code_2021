"""Solution to day 23


Other options: DFS from Yibo here: https://github.com/zybmax/adventofcode/blob/main/problems_2021/day23/solution_part2.py
4.6s run time

"""
import sys
from pathlib import Path
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple

import networkx as nx
from absl import logging

sys.setrecursionlimit(5000)

_Position = Tuple[int, int]


class AmphipodConfig:
    _HALLWAYS_POS = [(1, 1), (1, 2), (1, 4), (1, 6), (1, 8), (1, 10), (1, 11)]
    _POSITION_BY_ROOMS = {0: [(2, 3), (3, 3)], 1: [(2, 5), (3, 5)], 2: [(2, 7), (3, 7)], 3: [(2, 9), (3, 9)]}
    _EXPECTED_COLS_BY_CHAR = {"A": 3, "B": 5, "C": 7, "D": 9}
    _EXPECTED_CHAR_BY_COLS = {3: "A", 5: "B", 7: "C", 9: "D"}
    _ROOM_ID_BY_COL = {3: 0, 5: 1, 7: 2, 9: 3}
    _ENERGY_PER_STEP_BY_COLORS = {"A": 1, "B": 10, "C": 100, "D": 1000}

    """A class that describes pos config of the amphipods positions."""

    def __init__(self, hallway: str, rooms: List[str]):
        self._hallway = hallway
        self._rooms = rooms
        self._all_feasible_pos = AmphipodConfig._HALLWAYS_POS.copy()
        for room_positions in AmphipodConfig._POSITION_BY_ROOMS.values():
            self._all_feasible_pos.extend(room_positions)

    @property
    def neighbors_and_energy(self) -> List[Tuple["AmphipodConfig", int]]:
        res = []
        for pos, cc in self.positions.items():
            if cc == ".":
                continue
            else:
                if pos in AmphipodConfig._HALLWAYS_POS:
                    new_positions = self.positions.copy()
                    # From hallways to a room.
                    col_idx = AmphipodConfig._EXPECTED_COLS_BY_CHAR[cc]
                    if self._is_path_clear(src=pos, dst=(3, col_idx)):
                        assert new_positions[(2, col_idx)] == "."
                        new_positions[(3, col_idx)] = cc
                        new_positions[pos] = "."
                        energy = self._compute_energy(src=pos, dst=(3, col_idx), color=cc)
                        hallway, rooms = AmphipodConfig._convert_position_to_strings(new_positions)
                        res.append((AmphipodConfig(hallway=hallway, rooms=rooms), energy))
                    elif self._is_path_clear(src=pos, dst=(2, col_idx)):
                        # Don't enter we have a different type there.
                        if self.positions[(3, col_idx)] != cc:
                            continue
                        else:
                            new_positions[(2, col_idx)] = cc
                            new_positions[pos] = "."
                            energy = self._compute_energy(src=pos, dst=(2, col_idx), color=cc)
                            hallway, rooms = AmphipodConfig._convert_position_to_strings(new_positions)
                            res.append((AmphipodConfig(hallway=hallway, rooms=rooms), energy))
                    else:
                        continue
                else:
                    r, c = pos
                    if r == 3:
                        if self.positions[(2, c)] != ".":
                            # Don't move if we have somebody to go first
                            continue

                        elif self.positions[pos] == AmphipodConfig._EXPECTED_CHAR_BY_COLS[c]:
                            continue

                    elif r == 2:
                        if (
                            self.positions[(3, c)] == AmphipodConfig._EXPECTED_CHAR_BY_COLS[c]
                            and self.positions[(2, c)] == AmphipodConfig._EXPECTED_CHAR_BY_COLS[c]
                        ):
                            continue

                    candidate_dsts = [
                        x
                        for x in self._all_feasible_pos
                        if x != pos and self.positions[x] == "." and self._is_path_clear(src=pos, dst=x)
                    ]
                    for dst in candidate_dsts:
                        r_dst, c_dst = dst
                        if r_dst == 2 and self.positions[(3, c_dst)] == ".":
                            # Don't move to a configuration that generates a hole in the room.
                            continue

                        if r_dst >= 2:
                            if AmphipodConfig._EXPECTED_CHAR_BY_COLS[c_dst] != cc:
                                # Don't go to wrong cells.
                                continue
                            if (
                                r_dst == 2
                                and self.positions[(3, c_dst)] != AmphipodConfig._EXPECTED_CHAR_BY_COLS[c_dst]
                            ):
                                # Only go in if there is another ampphipod of the same type.
                                continue

                        energy = self._compute_energy(src=pos, dst=dst, color=cc)
                        new_positions = self.positions.copy()
                        new_positions[dst] = cc
                        new_positions[pos] = "."
                        hallway, rooms = AmphipodConfig._convert_position_to_strings(new_positions)
                        res.append((AmphipodConfig(hallway=hallway, rooms=rooms), energy))
        return res

    @property
    def positions(self) -> Dict[_Position, str]:
        res = {}
        for idx, c in enumerate(self._hallway):
            res[(1, idx + 1)] = c

        for room_idx, positions_in_room in self._POSITION_BY_ROOMS.items():
            for c, pos in zip(self._rooms[room_idx], positions_in_room):
                res[pos] = c

        return res

    def _is_path_clear(self, src: _Position, dst: _Position) -> bool:
        # Check for the clear path (not include the source but include the destination.)
        r_src, c_src = src
        r_dst, c_dst = dst
        all_coords = [(r, c_src) for r in range(r_src, 0, -1)]
        all_coords.extend([(1, c) for c in range(min(c_src, c_dst), max(c_src, c_dst) + 1)])
        all_coords.extend([(r, c_dst) for r in range(1, r_dst + 1)])
        try:
            if all(self.positions[c] == "." for c in all_coords if c != src):
                return True
            else:
                return False
        except RecursionError as e:
            print(self)
            print("Hit this")

    @staticmethod
    def _compute_energy(src: _Position, dst: _Position, color: str) -> int:
        r_src, c_src = src
        r_src_prj, c_src_prj = 1, c_src
        r_dst, c_dst = dst
        r_dst_prj, c_dst_prj = 1, c_dst
        path_len = r_src - r_src_prj + r_dst - r_dst_prj + abs(c_dst_prj - c_src_prj)
        return path_len * AmphipodConfig._ENERGY_PER_STEP_BY_COLORS[color]

    def __repr__(self):
        hall_way_str, room_str = self._convert_position_to_strings(self.positions)
        return f"AmphipodConfig(hallway = {hall_way_str}, rooms = {room_str}"

    @staticmethod
    def _convert_position_to_strings(positions: Dict[_Position, str]) -> Tuple[str, List[str]]:
        hall_way_str = "".join(positions[(1, i)] for i in range(1, 12))
        room_strs = [
            positions[(2, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            + positions[(3, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            for c in ["A", "B", "C", "D"]
        ]
        return hall_way_str, room_strs

    @staticmethod
    def _position_to_type(idx: int) -> str:
        return ["A", "B", "C", "D"][idx // 2]

    def __eq__(self, other: "AmphipodConfig") -> bool:
        return True if self.positions == other.positions else False

    @property
    def values(self):
        return (
            self._hallway,
            self._rooms[0],
            self._rooms[1],
            self._rooms[2],
            self._rooms[3],
        )


class AmphipodConfig2(AmphipodConfig):
    _POSITION_BY_ROOMS = {
        0: [(2, 3), (3, 3), (4, 3), (5, 3)],
        1: [(2, 5), (3, 5), (4, 5), (5, 5)],
        2: [(2, 7), (3, 7), (4, 7), (5, 7)],
        3: [(2, 9), (3, 9), (4, 9), (5, 9)],
    }

    """A class that describes pos config of the amphipods positions."""

    def __init__(self, hallway: str, rooms: List[str]):
        super().__init__(hallway=hallway, rooms=rooms)

    @staticmethod
    def _convert_position_to_strings(positions: Dict[_Position, str]) -> Tuple[str, List[str]]:
        hall_way_str = "".join(positions[(1, i)] for i in range(1, 12))
        room_strs = [
            positions[(2, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            + positions[(3, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            + positions[(4, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            + positions[(5, AmphipodConfig._EXPECTED_COLS_BY_CHAR[c])]
            for c in ["A", "B", "C", "D"]
        ]

        return hall_way_str, room_strs

    @property
    def neighbors_and_energy(self) -> List[Tuple["AmphipodConfig2", int]]:
        res = []
        for pos, cc in self.positions.items():
            if cc == ".":
                continue
            else:
                if pos in self._HALLWAYS_POS:
                    new_positions = self.positions.copy()
                    # From hallways to a room.
                    col_idx = AmphipodConfig._EXPECTED_COLS_BY_CHAR[cc]
                    if self._is_path_clear(src=pos, dst=(5, col_idx)):
                        new_positions[(5, col_idx)] = cc
                        new_positions[pos] = "."
                        energy = self._compute_energy(src=pos, dst=(5, col_idx), color=cc)
                        hallway, rooms = self._convert_position_to_strings(new_positions)
                        res.append((AmphipodConfig2(hallway=hallway, rooms=rooms), energy))
                    else:
                        for k in range(2, 5):
                            if self._is_path_clear(src=pos, dst=(k, col_idx)):
                                if any(self.positions[(l, col_idx)] != cc for l in range(k + 1, 6)):
                                    continue
                                else:
                                    new_positions[(k, col_idx)] = cc
                                    new_positions[pos] = "."
                                    energy = self._compute_energy(src=pos, dst=(k, col_idx), color=cc)
                                    hallway, rooms = self._convert_position_to_strings(new_positions)
                                    res.append((AmphipodConfig2(hallway=hallway, rooms=rooms), energy))
                else:
                    r, c = pos
                    block = False
                    for r_up in range(r - 1, 1, -1):
                        if self.positions[(r_up, c)] != ".":
                            block = True
                            break

                    if block:
                        continue

                    # Don't move out if we are already good
                    if all(self.positions[r_down, c] == self._EXPECTED_CHAR_BY_COLS[c] for r_down in range(r, 6)):
                        continue

                    candidate_dsts = [
                        x
                        for x in self._all_feasible_pos
                        if x != pos and self.positions[x] == "." and self._is_path_clear(src=pos, dst=x)
                    ]
                    for dst in candidate_dsts:
                        r_dst, c_dst = dst
                        # Make sure we move all the way
                        if c_dst in [3, 5, 7, 9] and any(
                            self.positions[(r_down, c_dst)] == "." for r_down in range(r_dst + 1, 6)
                        ):
                            continue

                        if r_dst >= 2:
                            if self._EXPECTED_CHAR_BY_COLS[c_dst] != cc:
                                # Don't go to wrong cells.
                                continue

                            if any(
                                self.positions[(r_down, c_dst)] != AmphipodConfig._EXPECTED_CHAR_BY_COLS[c_dst]
                                for r_down in range(r_dst + 1, 6)
                            ):
                                # Only go in if there is another ampphipod of the same type.
                                continue

                        energy = self._compute_energy(src=pos, dst=dst, color=cc)
                        new_positions = self.positions.copy()
                        new_positions[dst] = cc
                        new_positions[pos] = "."
                        hallway, rooms = self._convert_position_to_strings(new_positions)
                        res.append((AmphipodConfig2(hallway=hallway, rooms=rooms), energy))
        return res


def day23() -> None:
    day23_input_file_name = Path(__file__).parent.parent / "data" / "day23_input.txt"
    with open(day23_input_file_name, "r") as file:
        all_lines = [l[:-1] for l in file.readlines()]

    _part1(all_lines)

    _part2(all_lines)


def _part1(all_lines: List[str]) -> None:
    """
    --- Day 23: Amphipod ---
    A group of amphipods notice your fancy submarine and flag you down. "With such an impressive shell," one amphipod
    says, "surely you can help us with a question that has stumped our best scientists."

    They go on to explain that a group of timid, stubborn amphipods live in a nearby burrow. Four types of amphipods
    live there: Amber (A), Bronze (B), Copper (C), and Desert (D). They live in a burrow that consists of a hallway and
    four side rooms. The side rooms are initially full of amphipods, and the hallway is initially empty.

    They give you a diagram of the situation (your puzzle input), including locations of each amphipod (A, B, C, or D,
    each of which is occupying an otherwise open space), walls (#), and open space (.).

    For example:

    #############
    #...........#
    ###B#C#B#D###
      #A#D#C#A#
      #########
    The amphipods would like a method to organize every amphipod into side rooms so that each side room contains one
    type of amphipod and the types are sorted A-D going left to right, like this:

    #############
    #...........#
    ###A#B#C#D###
      #A#B#C#D#
      #########
    Amphipods can move up, down, left, or right so long as they are moving into an unoccupied open space. Each type of
    amphipod requires a different amount of energy to move one step: Amber amphipods require 1 energy per step, Bronze
    amphipods require 10 energy, Copper amphipods require 100, and Desert ones require 1000. The amphipods would like
    you to find a way to organize the amphipods that requires the least total energy.

    However, because they are timid and stubborn, the amphipods have some extra rules:

    Amphipods will never stop on the space immediately outside any room. They can move into that space so long as they
    immediately continue moving. (Specifically, this refers to the four open spaces in the hallway that are directly
    above an amphipod starting position.)
    Amphipods will never move from the hallway into a room unless that room is their destination room and that room
    contains no amphipods which do not also have that room as their own destination. If an amphipod's starting room is
    not its destination room, it can stay in that room until it leaves the room. (For example, an Amber amphipod will
    not move from the hallway into the right three rooms, and will only move into the leftmost room if that room is
    empty or if it only contains other Amber amphipods.)
    Once an amphipod stops moving in the hallway, it will stay in that spot until it can move into a room. (That is,
    once any amphipod starts moving, any other amphipods currently in the hallway are locked in place and will not move
    again until they can move fully into a room.)
    In the above example, the amphipods can be organized using a minimum of 12521 energy. One way to do this is shown
    below.

    Starting configuration:

    #############
    #...........#
    ###B#C#B#D###
      #A#D#C#A#
      #########
    One Bronze amphipod moves into the hallway, taking 4 steps and using 40 energy:

    #############
    #...B.......#
    ###B#C#.#D###
      #A#D#C#A#
      #########
    The only Copper amphipod not in its side room moves there, taking 4 steps and using 400 energy:

    #############
    #...B.......#
    ###B#.#C#D###
      #A#D#C#A#
      #########
    A Desert amphipod moves out of the way, taking 3 steps and using 3000 energy, and then the Bronze amphipod takes its
    place, taking 3 steps and using 30 energy:

    #############
    #.....D.....#
    ###B#.#C#D###
      #A#B#C#A#
      #########
    The leftmost Bronze amphipod moves to its room using 40 energy:

    #############
    #.....D.....#
    ###.#B#C#D###
      #A#B#C#A#
      #########
    Both amphipods in the rightmost room move into the hallway, using 2003 energy in total:

    #############
    #.....D.D.A.#
    ###.#B#C#.###
      #A#B#C#.#
      #########
    Both Desert amphipods move into the rightmost room using 7000 energy:

    #############
    #.........A.#
    ###.#B#C#D###
      #A#B#C#D#
      #########
    Finally, the last Amber amphipod moves into its room, using 8 energy:

    #############
    #...........#
    ###A#B#C#D###
      #A#B#C#D#
      #########
    What is the least energy required to organize the amphipods?

    Your puzzle answer was 15111.
    """
    first_config = _parse_room_config(all_lines)
    last_node = AmphipodConfig(hallway="...........", rooms=["AA", "BB", "CC", "DD"])
    graph = nx.Graph()
    _add_node_to_graph(graph, first_config)
    print(
        f"p1 res = {nx.dijkstra_path_length(graph, source=first_config.values, target=last_node.values, weight='weight')}"
    )


def _parse_room_config(all_lines: List[str]) -> AmphipodConfig:
    pos_idx_by_room_idx = {1: 3, 2: 5, 3: 7, 4: 9}
    rooms = []
    for room in pos_idx_by_room_idx.keys():
        rooms.append(all_lines[2][pos_idx_by_room_idx[room]] + all_lines[3][pos_idx_by_room_idx[room]])
    return AmphipodConfig(hallway=all_lines[1][1:-1], rooms=rooms)


def _add_node_to_graph(gr: nx.Graph, config: AmphipodConfig) -> Optional[int]:
    if config.values in gr.nodes:
        return

    gr.add_node(config.values)
    for neighbor, energy in config.neighbors_and_energy:
        if neighbor.values not in gr.nodes:
            _add_node_to_graph(gr, neighbor)

        gr.add_edge(config.values, neighbor.values, weight=energy)
        gr.add_edge(neighbor.values, config.values, weight=energy)


def _part2(all_lines: List[str]) -> None:
    """
    --- Part Two ---
    As you prepare to give the amphipods your solution, you notice that the diagram they handed you was actually folded
    up. As you unfold it, you discover an extra part of the diagram.

    Between the first and second lines of text that contain amphipod starting positions, insert the following lines:

      #D#C#B#A#
      #D#B#A#C#
    So, the above example now becomes:

    #############
    #...........#
    ###B#C#B#D###
      #D#C#B#A#
      #D#B#A#C#
      #A#D#C#A#
      #########
    The amphipods still want to be organized into rooms similar to before:

    #############
    #...........#
    ###A#B#C#D###
      #A#B#C#D#
      #A#B#C#D#
      #A#B#C#D#
      #########
    In this updated example, the least energy required to organize these amphipods is 44169:

    #############
    #...........#
    ###B#C#B#D###
      #D#C#B#A#
      #D#B#A#C#
      #A#D#C#A#
      #########

    #############
    #..........D#
    ###B#C#B#.###
      #D#C#B#A#
      #D#B#A#C#
      #A#D#C#A#
      #########

    #############
    #A.........D#
    ###B#C#B#.###
      #D#C#B#.#
      #D#B#A#C#
      #A#D#C#A#
      #########

    #############
    #A........BD#
    ###B#C#.#.###
      #D#C#B#.#
      #D#B#A#C#
      #A#D#C#A#
      #########

    #############
    #A......B.BD#
    ###B#C#.#.###
      #D#C#.#.#
      #D#B#A#C#
      #A#D#C#A#
      #########

    #############
    #AA.....B.BD#
    ###B#C#.#.###
      #D#C#.#.#
      #D#B#.#C#
      #A#D#C#A#
      #########

    #############
    #AA.....B.BD#
    ###B#.#.#.###
      #D#C#.#.#
      #D#B#C#C#
      #A#D#C#A#
      #########

    #############
    #AA.....B.BD#
    ###B#.#.#.###
      #D#.#C#.#
      #D#B#C#C#
      #A#D#C#A#
      #########

    #############
    #AA...B.B.BD#
    ###B#.#.#.###
      #D#.#C#.#
      #D#.#C#C#
      #A#D#C#A#
      #########

    #############
    #AA.D.B.B.BD#
    ###B#.#.#.###
      #D#.#C#.#
      #D#.#C#C#
      #A#.#C#A#
      #########

    #############
    #AA.D...B.BD#
    ###B#.#.#.###
      #D#.#C#.#
      #D#.#C#C#
      #A#B#C#A#
      #########

    #############
    #AA.D.....BD#
    ###B#.#.#.###
      #D#.#C#.#
      #D#B#C#C#
      #A#B#C#A#
      #########

    #############
    #AA.D......D#
    ###B#.#.#.###
      #D#B#C#.#
      #D#B#C#C#
      #A#B#C#A#
      #########

    #############
    #AA.D......D#
    ###B#.#C#.###
      #D#B#C#.#
      #D#B#C#.#
      #A#B#C#A#
      #########

    #############
    #AA.D.....AD#
    ###B#.#C#.###
      #D#B#C#.#
      #D#B#C#.#
      #A#B#C#.#
      #########

    #############
    #AA.......AD#
    ###B#.#C#.###
      #D#B#C#.#
      #D#B#C#.#
      #A#B#C#D#
      #########

    #############
    #AA.......AD#
    ###.#B#C#.###
      #D#B#C#.#
      #D#B#C#.#
      #A#B#C#D#
      #########

    #############
    #AA.......AD#
    ###.#B#C#.###
      #.#B#C#.#
      #D#B#C#D#
      #A#B#C#D#
      #########

    #############
    #AA.D.....AD#
    ###.#B#C#.###
      #.#B#C#.#
      #.#B#C#D#
      #A#B#C#D#
      #########

    #############
    #A..D.....AD#
    ###.#B#C#.###
      #.#B#C#.#
      #A#B#C#D#
      #A#B#C#D#
      #########

    #############
    #...D.....AD#
    ###.#B#C#.###
      #A#B#C#.#
      #A#B#C#D#
      #A#B#C#D#
      #########

    #############
    #.........AD#
    ###.#B#C#.###
      #A#B#C#D#
      #A#B#C#D#
      #A#B#C#D#
      #########

    #############
    #..........D#
    ###A#B#C#.###
      #A#B#C#D#
      #A#B#C#D#
      #A#B#C#D#
      #########

    #############
    #...........#
    ###A#B#C#D###
      #A#B#C#D#
      #A#B#C#D#
      #A#B#C#D#
      #########
    Using the initial configuration from the full diagram, what is the least energy required to organize the amphipods?

    Your puzzle answer was 47625.
    """
    first_node = _parse_room_config_p2(all_lines)
    last_node = AmphipodConfig2(hallway="...........", rooms=["AAAA", "BBBB", "CCCC", "DDDD"])
    graph = nx.Graph()
    _add_node_to_graph_p2(graph, first_node)

    print(
        f"p2 res = {nx.dijkstra_path_length(graph, source=first_node.values, target=last_node.values, weight='weight')}"
    )


def _parse_room_config_p2(all_lines: List[str]) -> AmphipodConfig2:
    pos_idx_by_room_idx = {1: 3, 2: 5, 3: 7, 4: 9}
    first_new_line = "  #D#C#B#A#"
    second_new_line = "  #D#B#A#C#"
    rooms = []
    for room in pos_idx_by_room_idx.keys():
        col_idx = pos_idx_by_room_idx[room]
        rooms.append(all_lines[2][col_idx] + first_new_line[col_idx] + second_new_line[col_idx] + all_lines[3][col_idx])
    return AmphipodConfig2(hallway=all_lines[1][1:-1], rooms=rooms)


def _add_node_to_graph_p2(gr: nx.Graph, config: AmphipodConfig2) -> Optional[int]:
    if config.values in gr.nodes:
        return

    if len(gr.nodes) % 10000 == 0:
        print(f"Added {len(gr.nodes)} nodes to graph.")

    gr.add_node(config.values)
    for neighbor, energy in config.neighbors_and_energy:
        if neighbor.values not in gr.nodes:
            _add_node_to_graph_p2(gr, neighbor)

        gr.add_edge(config.values, neighbor.values, weight=energy)
        gr.add_edge(neighbor.values, config.values, weight=energy)
