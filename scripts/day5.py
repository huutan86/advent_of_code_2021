"""Source code to solve day 5 challenge."""
from pathlib import Path
from typing import List
from typing import Tuple

import numpy as np
from absl import logging

from scripts._util import time_it


def _proc_input(data: List[str]) -> Tuple[List[int], List[int]]:
    return tuple([int(v) for v in x.split(",")] for x in data)


def day5() -> None:
    day5_input_file_name = Path(__file__).parent.parent / "data" / "day5_input.txt"
    with open(day5_input_file_name, "r") as file:
        coords = [_proc_input(l.strip().split(" -> ")) for l in file.readlines()]

    with time_it("part 1") as _:
        _part1(coords)

    with time_it("part 2") as _:
        _part2(coords)


def _get_array_shape(coords: Tuple[List[int], List[int]]) -> Tuple[int, int]:
    all_x = [pt[0] for line in coords for pt in line]
    all_y = [pt[1] for line in coords for pt in line]
    return max(all_y) + 1, max(all_x) + 1


def _generate_matrix_from_vertical_and_horizontal_lines(coords: Tuple[List[int], List[int]]) -> np.ndarray:
    data = np.zeros(_get_array_shape(coords), dtype=np.int)
    for line in coords:
        (x1, y1), (x2, y2) = line
        if x1 == x2 or y1 == y2:
            x_range = range(min(x1, x2), max(x1, x2) + 1)
            y_range = range(min(y1, y2), max(y1, y2) + 1)
            data[y_range, x_range] = data[y_range, x_range] + 1
    return data


def _part1(coords: Tuple[List[int], List[int]]) -> None:
    """
    --- Day 5: Hydrothermal Venture ---
    You come across a field of hydrothermal vents on the ocean floor! These vents constantly produce large, opaque
    clouds, so it would be best to avoid them if possible.

    They tend to form in lines; the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for
    you to review. For example:

    0,9 -> 5,9
    8,0 -> 0,8
    9,4 -> 3,4
    2,2 -> 2,1
    7,0 -> 7,4
    6,4 -> 2,0
    0,9 -> 2,9
    3,4 -> 1,4
    0,0 -> 8,8
    5,5 -> 8,2
    Each line of vents is given as a line segment in the format x1,y1 -> x2,y2 where x1,y1 are the coordinates of one
    end the line segment and x2,y2 are the coordinates of the other end. These line segments include the points at both
    ends. In other words:

    An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
    An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.
    For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.

    So, the horizontal and vertical lines from the above list would produce the following diagram:

    .......1..
    ..1....1..
    ..1....1..
    .......1..
    .112111211
    ..........
    ..........
    ..........
    ..........
    222111....
    In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9. Each position is shown as the number
    of lines which cover that point or . if no line covers that point. The top-left pair of 1s, for example, comes from
    2,2 -> 2,1; the very bottom row is formed by the overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.

    To avoid the most dangerous areas, you need to determine the number of points where at least two lines overlap. In
    the above example, this is anywhere in the diagram with a 2 or larger - a total of 5 points.

    Consider only horizontal and vertical lines. At how many points do at least two lines overlap?

    Your puzzle answer was 5774.
    """
    data = _generate_matrix_from_vertical_and_horizontal_lines(coords)
    logging.info(f"p1 res = {np.sum(data >= 2)}")


def _part2(coords: Tuple[List[int], List[int]]) -> None:
    """
    --- Part Two ---
    Unfortunately, considering only horizontal and vertical lines doesn't give you the full picture; you need to also
    consider diagonal lines.

    Because of the limits of the hydrothermal vent mapping system, the lines in your list will only ever be horizontal,
    vertical, or a diagonal line at exactly 45 degrees. In other words:

    An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
    An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
    Considering all lines from the above example would now produce the following diagram:

    1.1....11.
    .111...2..
    ..2.1.111.
    ...1.2.2..
    .112313211
    ...1.2....
    ..1...1...
    .1.....1..
    1.......1.
    222111....
    You still need to determine the number of points where at least two lines overlap. In the above example, this is
    still anywhere in the diagram with a 2 or larger - now a total of 12 points.

    Consider all of the lines. At how many points do at least two lines overlap?

    Your puzzle answer was 18423.
    """
    data = _generate_matrix_from_vertical_and_horizontal_lines(coords)
    for line in coords:
        (x1, y1), (x2, y2) = line
        min_y = min((y1, y2))
        max_y = max((y1, y2))
        if max_y != min_y:
            ang = (x2 - x1) / (y2 - y1)
            x_range = range(min((x1, x2)), max((x1, x2)) + 1)
            if ang == 1:
                y_range = range(min_y, max_y + 1)
                # See indexing with multi-dimensional array here https://numpy.org/doc/stable/user/basics.indexing.html
                data[y_range, x_range] = data[y_range, x_range] + 1

            if ang == -1:
                y_range = range(max_y, min_y - 1, -1)
                data[y_range, x_range] = data[y_range, x_range] + 1

    logging.info(f"p2 res = {np.sum(data >= 2)}")
