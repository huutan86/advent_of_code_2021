"""Source code to solve day 12 challenge."""
# None recursive solution here: https://github.com/zecookiez/AdventOfCode2021/blob/main/day14.py
from collections import Counter
from functools import lru_cache
from pathlib import Path
from typing import List

from absl import logging

from scripts._util import time_it


def _proc_line(line: str) -> List[int]:
    return tuple(int(x) for x in line.split(","))


day14_input_file_name = Path(__file__).parent.parent / "data" / "day14_input.txt"
with open(day14_input_file_name, "r") as file:
    all_lines = [l.strip() for l in file.readlines()]

chain = all_lines[0]
rules = {}
for l in all_lines[2:]:
    k, v = l.split(" -> ")
    rules[k] = v


def day14() -> None:
    with time_it("part 1") as _:
        _part1(chain)

    with time_it("part 2") as _:
        _part2(chain)


@lru_cache(maxsize=None)
def _count_characters_in_pair_children(char_pair: str, step_lefts: int) -> Counter:
    if step_lefts == 0:
        return Counter(char_pair)
    else:
        if char_pair in rules:
            out_res = Counter()
            new_char = rules[char_pair]
            out_res += _count_characters_in_pair_children(char_pair[0] + new_char, step_lefts=step_lefts - 1)
            out_res += _count_characters_in_pair_children(new_char + char_pair[1], step_lefts=step_lefts - 1)
            out_res[new_char] -= 1
        else:
            out_res = Counter(char_pair)
        return out_res


def _count_characters_in_string(chain: str, num_steps: int) -> Counter:
    out_res = Counter()
    for id in range(len(chain) - 1):
        out_res += _count_characters_in_pair_children(chain[id : id + 2], step_lefts=num_steps)

    # The join between two neighboring pairs are counted twice, account for it.
    for c in chain[1:-1]:
        out_res[c] -= 1
    return out_res


def _part1(chain: str) -> None:
    """
    --- Day 14: Extended Polymerization ---
    The incredible pressures at this depth are starting to put a strain on your submarine. The submarine has
    polymerization equipment that would produce suitable materials to reinforce the submarine, and the nearby
    volcanically-active caves should even have the necessary input elements in sufficient quantities.

    The submarine manual contains instructions for finding the optimal polymer formula; specifically, it offers a
    polymer template and a list of pair insertion rules (your puzzle input). You just need to work out what polymer
    would result after repeating the pair insertion process a few times.

    For example:

    NNCB

    CH -> B
    HH -> N
    CB -> H
    NH -> C
    HB -> C
    HC -> B
    HN -> C
    NN -> C
    BH -> H
    NC -> B
    NB -> B
    BN -> B
    BB -> N
    BC -> B
    CC -> N
    CN -> C
    The first line is the polymer template - this is the starting point of the process.

    The following section defines the pair insertion rules. A rule like AB -> C means that when elements A and B are
    immediately adjacent, element C should be inserted between them. These insertions all happen simultaneously.

    So, starting with the polymer template NNCB, the first step simultaneously considers all three pairs:

    The first pair (NN) matches the rule NN -> C, so element C is inserted between the first N and the second N.
    The second pair (NC) matches the rule NC -> B, so element B is inserted between the N and the C.
    The third pair (CB) matches the rule CB -> H, so element H is inserted between the C and the B.
    Note that these pairs overlap: the second element of one pair is the first element of the next pair. Also, because
    all pairs are considered simultaneously, inserted elements are not considered to be part of a pair until the next
    step.

    After the first step of this process, the polymer becomes NCNBCHB.

    Here are the results of a few steps using the above rules:

    Template:     NNCB
    After step 1: NCNBCHB
    After step 2: NBCCNBBBCBHCB
    After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
    After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
    This polymer grows quickly. After step 5, it has length 97; After step 10, it has length 3073. After step 10, B
    occurs 1749 times, C occurs 298 times, H occurs 161 times, and N occurs 865 times; taking the quantity of the most
    common element (B, 1749) and subtracting the quantity of the least common element (H, 161) produces 1749 - 161 =
    1588.

    Apply 10 steps of pair insertion to the polymer template and find the most and least common elements in the result.
    What do you get if you take the quantity of the most common element and subtract the quantity of the least common
    element?

    Your puzzle answer was 3831.
    """
    res = _count_characters_in_string(chain, num_steps=10)
    logging.info(f"P2 res = {max(res.values()) - min(res.values())}")


def _part2(chain: str) -> None:
    """
    --- Part Two ---
    The resulting polymer isn't nearly strong enough to reinforce the submarine. You'll need to run more steps of the
    pair insertion process; a total of 40 steps should do it.

    In the above example, the most common element is B (occurring 2192039569602 times) and the least common element is H
    occurring 3849876073 times); subtracting these produces 2188189693529.

    Apply 40 steps of pair insertion to the polymer template and find the most and least common elements in the result.
    What do you get if you take the quantity of the most common element and subtract the quantity of the least common element?

    Your puzzle answer was 5725739914282.
    """
    res = _count_characters_in_string(chain, num_steps=40)
    logging.info(f"P2 res = {max(res.values()) - min(res.values())}")
