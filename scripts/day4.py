"""Source code to solve day 4 challenge."""
from pathlib import Path
from typing import List

import numpy as np
from absl import logging

from scripts._util import time_it


def day4() -> None:
    day4_input_file_name = Path(__file__).parent.parent / "data" / "day4_input.txt"
    with open(day4_input_file_name, "r") as file:
        all_lines = [l.strip() for l in file.readlines()]
    call_seq = [int(x) for x in all_lines[0].split(",")]
    num_line_per_table = 6
    num_tables = int((len(all_lines) - 1) / num_line_per_table)
    all_tables = []
    for table_idx in range(num_tables):
        first_row = 2 + table_idx * num_line_per_table
        all_tables.append(
            np.array([[int(item) for item in line.split()] for line in all_lines[first_row : first_row + 5]])
        )

    with time_it("part 1") as _:
        _part1(call_seq, all_tables)

    with time_it("part 2") as _:
        _part2(call_seq, all_tables)


def _part1(call_seq: List[int], all_tables: List[np.ndarray]) -> None:
    """
    --- Day 4: Giant Squid ---
    You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that you can't see any
    sunlight. What you can see, however, is a giant squid that has attached itself to the outside of your submarine.

    Maybe it wants to play bingo?

    Bingo is played on a set of boards each consisting of a 5x5 grid of numbers. Numbers are chosen at random, and the
    chosen number is marked on all boards on which it appears. (Numbers may not appear on all boards.) If all numbers in
     any row or any column of a board are marked, that board wins. (Diagonals don't count.)

    The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass the time. It
    automatically generates a random order in which to draw numbers and a random set of boards (your puzzle input). For
    example:

    7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

    22 13 17 11  0
     8  2 23  4 24
    21  9 14 16  7
     6 10  3 18  5
     1 12 20 15 19

     3 15  0  2 22
     9 18 13 17  5
    19  8  7 25 23
    20 11 10 24  4
    14 21 16 12  6

    14 21 17 24  4
    10 16 15  9 19
    18  8 23 26 20
    22 11 13  6  5
     2  0 12  3  7
    After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners, but the boards are marked as
    follows (shown here adjacent to each other to save space):

    22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:

    22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    Finally, 24 is drawn:

    22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
     8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
     6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
     1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    At this point, the third board wins because it has at least one complete row or column of marked numbers (in this
    case, the entire top row is marked: 14 21 17 24 4).

    The score of the winning board can now be calculated. Start by finding the sum of all unmarked numbers on that
    board; in this case, the sum is 188. Then, multiply that sum by the number that was just called when the board won,
    24, to get the final score, 188 * 24 = 4512.

    To guarantee victory against the giant squid, figure out which board will win first. What will your final score be
    if you choose that board?

    Your puzzle answer was 35711.
    """
    table_data = {idx: table for idx, table in enumerate(all_tables)}
    try:
        for item in call_seq:
            for idx, table in table_data.items():
                table[table == item] = 0
                if np.any(np.sum(table, axis=0) == 0) or np.any(np.sum(table, axis=1) == 0):
                    unmark = np.sum(table)
                    logging.info(f"table idx = {idx}, res = {unmark * item}")
                    raise StopIteration
    except StopIteration as e:
        pass


def _part2(call_seq: List[int], all_tables: List[np.ndarray]) -> None:
    """
    --- Part Two ---
    On the other hand, it might be wise to try a different strategy: let the giant squid win.

    You aren't sure how many bingo boards a giant squid could play at once, so rather than waste time counting its arms,
    the safe thing to do is to figure out which board will win last and choose that one. That way, no matter which
    boards it picks, it will win for sure.

    In the above example, the second board is the last to win, which happens after 13 is eventually called and its
    middle column is completely marked. If you were to keep playing until this point, the second board would have a sum
    of unmarked numbers equal to 148 for a final score of 148 * 13 = 1924.

    Figure out which board will win last. Once it wins, what would its final score be?

    Your puzzle answer was 5586.
    """
    table_data = {idx: table for idx, table in enumerate(all_tables)}
    unfinished_tables_idx = list(table_data.keys())
    found_val_by_table_idx = {}
    for item in call_seq:
        for idx in unfinished_tables_idx:
            if idx not in found_val_by_table_idx:
                table = all_tables[idx]
                table[table == item] = 0
                if np.any(np.sum(table, axis=0) == 0) or np.any(np.sum(table, axis=1) == 0):
                    unmark = np.sum(table)
                    found_val_by_table_idx[idx] = unmark * item
    logging.info(f"Part 2 res = {found_val_by_table_idx[list(found_val_by_table_idx.keys())[-1]]}")
